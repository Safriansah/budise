validasi();
function validasi(){
    var elements = document.getElementsByClassName("form-control");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Masukkan data sesuai dengan form yang tersedia");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }   
}