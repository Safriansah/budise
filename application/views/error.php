<body>
          <header style="height: 100px;" id="hero-area" data-stellar-background-ratio="0.5">    
                <!-- Navbar Start -->
                <nav class="navbar navbar-expand-lg scrolling-navbar fixed-top indigo">
                  <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <a href="<?php echo base_url() ?>" class="navbar-brand"><img class="img-fulid" src="<?php echo base_url() ?>assets/images/logo.png" height="40px" alt=""></a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="lnr lnr-menu"></i>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar">
                      <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>berita">Berita</a>
                        </li>
                        <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>galeri">Galeri</a>
                        </li>
                    </div>
                  </div>
          
                  <!-- Mobile Menu Start -->
                  <ul class="mobile-menu">
                     <li>
                        <a class="page-scroll" href="<?php echo base_url() ?>">Home</a>
                      </li>
                      <li>
                        <a class="page-scroll" href="<?php echo base_url() ?>berita">Berita</a>
                      </li>
                      <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>galeri">Galeri</a>
                        </li>
                  </ul>
                  <!-- Mobile Menu End -->
          
                </nav>
                <!-- Navbar End -->   
                <div class="container">      
                  <div class="row justify-content-md-center">
                    <div class="col-md-10">
                      <div class="contents text-center" style="height: 40px;">
                      </div>
                    </div>
                  </div> 
                </div>           
              </header>

              <section id="blog" class="section" 
              style="
                    min-height: 720px; 
                    background: url(<?php echo base_url() ?>assets/images/error.jpg) no-repeat;
                    background-size: 100% auto;">
              </section>