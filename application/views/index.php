
  <body>

    <!-- Header Section Start -->
    <header id="hero-area" data-stellar-background-ratio="0.5">    
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a href="<?php echo base_url() ?>" class="navbar-brand"><img class="img-fulid" src="<?php echo base_url() ?>assets/images/logo.png" height="40px" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <i class="lnr lnr-menu"></i>
            </button>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#hero-area">Home</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tentang Kami</a>
                <div class="dropdown-menu" style="padding:12px;font-size:14px" aria-labelledby="navbarDropdownMenuLink">
                  <a class="nav-link page-scroll" href="#motto">Motto</a>
                  <a class="nav-link page-scroll" href="#features">Keunggulan</a>
                  <a class="nav-link page-scroll" href="#portfolios">Galeri</a>
                  <a class="nav-link page-scroll" href="#sejarah">Sejarah</a>
                  <a class="nav-link page-scroll" href="#visimisi">Visi Misi</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#blog">Berita</a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="#contact">Hubungi Kami</a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="mobile-menu">
           <li>
              <a class="page-scroll" href="#hero-area">Home</a>
            </li>
            <li>
              <a class="page-scroll" href="#motto">Motto</a>
            </li>
            <li>
              <a class="page-scroll" href="#features">Keunggulan</a>
            </li>
            <li>
              <a class="page-scroll" href="#portfolios">Galeri</a>
            </li>
            <li>
              <a class="page-scroll" href="#sejarah">Sejarah</a>
            </li>
            <li>
              <a class="page-scroll" href="#visimisi">Visi Misi</a>
            </li>
            <li >
              <a class="page-scroll" href="#blog">Berita</a>
            </li>
            <li>
              <a class="page-scroll" href="#contact">Hubungi Kami</a>
            </li>
        </ul>
        <!-- Mobile Menu End -->

      </nav>
      <!-- Navbar End -->   
      <div class="container">      
        <div class="row justify-content-md-center">
          <div class="col-md-10">
            <div class="contents text-center">
              <h1 class="wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0.3s">BSE Transportindo</h1>
              <p class="lead  wow fadeIn" data-wow-duration="1000ms" data-wow-delay="400ms">Starting a business is a huge amount of hard work, You had better enjoy it!</p>
            </div>
          </div>
        </div> 
      </div>           
    </header>
    <!-- Header Section End --> 

    <!-- Services Section Start -->
    <section id="motto" class="section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title wow fadeIn" data-wow-duration="1000ms" data-wow-delay="0.3s">Motto Perusahaan Kami?</h2>
          <hr class="lines wow zoomIn" data-wow-delay="0.3s">
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.2s">
              <div class="icon">
                <i class="fa fa-cog"></i>
              </div>
              <h4>Intergritas</h4>
              <p>Setiap insan di perusahaan harus melakukan pekerjaannya dengan penuh tanggung jawab, professional dan penuh dedikasi.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="0.8s">
              <div class="icon">
                <i class="lnr lnr-code"></i>
              </div>
              <h4>Jujur</h4>
              <p>Setiap insan ditanamkan nilai-nilai kejujuran dalam menjalankan amanah dari perusahaan untuk memberikan pelayanan yang terbaik bagi seluruh costumer.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="item-boxes wow fadeInDown" data-wow-delay="1.2s">
              <div class="icon">
                <i class="fa fa-handshake-o"></i>
              </div>
              <h4>Peduli</h4>
              <p>LSetiap insan di perusahaan bertanggung jawab penuh untuk melayani, memperhatikan dan membantu costumer sesuai dengan kebutuhannya.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Services Section End -->

    <!-- Features Section Start -->
    <section id="features" class="section" data-stellar-background-ratio="0.2">
      <div class="container" style="background: #123;padding: 32px;">
        <div class="section-header">          
          <h2 class="section-title">Keunggulan Perusahaan Kami?</h2>
          <hr class="lines">
        </div>
        <div class="row">
          <div class="col-lg-8 col-md-12 col-xs-12">
            <div class="container">
              <div class="row">
                 <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="fa fa-money fa-5"></i>
                    </span>
                    <div class="text">
                      <h4>Harga Bersahabat</h4>
                      <p>Budi Setiawan Enterprise Transportindo menawarkan harga yang lebih murah daripada jasa jasa logistic lainnya.</p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="fa fa-truck fa-5"></i>
                    </span>
                    <div class="text">
                      <h4>Pengiriman Cepat</h4>
                      <p>Budi Setiawan Enterprise Transportindo menjanjikan proses pengiriman yang tepat waktu.</p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="fa fa-lock fa-5"></i>
                    </span>
                    <div class="text">
                      <h4>Barang Dijamin Aman</h4>
                      <p>Budi Setiawan Enterprise Transportindo menjamin keamanan barang anda karena kurir yang kami digunakan adalah orang orang yang bekerja secara profesianal dan dapat dipercaya.</p>
                    </div>
                  </div>
                  <div class="col-lg-6 col-sm-6 col-xs-12 box-item">
                    <span class="icon">
                      <i class="fa fa-smile-o fa-5"></i>
                    </span>
                    <div class="text">
                      <h4>Pemecahan Solusi</h4>
                      <p>Budi Setiawan Enterprise Transportindo adalah solusi dikala anda bingung memilih jasa logistik yang tepat, karena Budi Setiawan Enterprise Transportindo memiliki karyawan dan kurir yang bekerja secara profesional.</p>
                    </div>
                  </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-xs-12">
            <div class="show-box">
              <img class="img-fulid" src="assets/images/kang.png" style="max-width:100%" alt="">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Features Section End -->    

    <!-- Portfolio Section -->
    <section id="portfolios" class="section">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Galeri Perusahaan</h2>
          <hr class="lines">
        </div>
        <div class="row"> 
          <!-- Portfolio Recent Projects -->
          <div id="portfolio" class="row">
          <?php
		        foreach($galeri as $d){
		      ?>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix development print">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="assets/images/galeri/<?php echo $d->GAMBAR ?>" alt="" />  
                  <a class="overlay lightbox" href="assets/images/galeri/<?php echo $d->GAMBAR ?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
          <?php } ?>
          </div>
        </div>
        <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <div class="controls text-center">
              <?php 
                if($countgaleri<1) echo"Galeri Kosong";
                else if($countgaleri>6) echo '<a class="btn btn-common" href="galeri" data-filter="all">Tampilkan Lebih Banyak.... </a>';
              ?>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
          </div>
        </div>
      </div>
      <!-- Container Ends -->
    </section>
    <!-- Portfolio Section Ends --> 

     <!-- Start Video promo Section -->
    <section id="sejarah" class="video-promo section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8" style="border: 4px dashed #fff;padding: 32px;border-radius: 32px;">
              <div class="video-promo-content text-center">
                <h2 class="wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Sejarah Perusahaan</h2>
                <p class="wow zoomIn text-justify" data-wow-duration="1000ms" data-wow-delay="100ms">Budi Setiawan Enterprise adalah suatu Cv. yang bergerak di berbagai macam jasa. seperti logistik, jurnalistik, dan lain sebagainya. Budi Setiawan Enterprise atau yang biasa disingkat BSE didirikan pada tahun 2016 oleh Budi Setiawan,STP., BSE memiliki kantor di Jalan Pulosari 3-j/17 B, Kelurahan Gunungsari, Surabaya. Budi Setiawan Enterprise awal mulanya hanya bergerak di bidang logistik karena melihat jasa pengiriman peti kemas di kota Surabaya yang masih kurang memadai dari segi armada, karena itu Budi Setiawan Enterprise membuka suatu perusahaan yang bernama Budi Setiawan Transportindo sebagai ikhtiyar untuk membantu proses pengiriman peti kemas yang notabene masih kurang di kota Surabaya. Pada awal berdirinya Budi Setiawan Transportindo bertindak sebagai pihak ketiga atau hanya menerima pesanan dari suatu perusahaan jasa pengiriman yang terkendala oleh kurangnya armada untuk mengirimkan barang. Tapi seiring berjalannya waktu Budi Setiawan Transportindo tidak lagi hanya berperan sebagai pihak ketiga karena sudah mulai banyak dikenal oleh perusahaan-perusahaan yang membutuhkan jasa pengiriman lewat peti kemas.</p>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Video Promo Section -->

    <!-- Start Pricing Table Section -->
    <div id="visimisi" class="section pricing-section">
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Visi dan Misi Perusahaan</h2>
          <hr class="lines">
        </div>

        <div class="row pricing-tables">
          <div class="col-md-6 col-sm-6 col-xl-6">
            <div class="pricing-table" >
              <div class="pricing-details">
                <h2 style="padding-top: 15px;padding-bottom: 15px;">Visi</h2>
                <!-- <span>$00</span> -->
                  <ol>
                    <li class="text-left">Menjadikan perusahaan yang kompetitif dan berorientasi pada kebaikan bersama.</li>
                    <br>
                    <li class="text-left">Terdepan dalam pelayanan yang berorientasi pada kepuasan partner.</li>
                  </ol>
            </div>
          </div>
          </div>

          <div class="col-md-6 col-sm-6 col-xl-6">
            <div class="pricing-table" >
              <div class="pricing-details">
                <h2 style="padding-top: 15px;padding-bottom: 15px;">Misi</h2>
                <!-- <span>$3.99</span> -->
                <ol>
                  <li class="text-left">Terbinannya kerjasama yang professional dengan semua pemamngku kepentingan (stakeholder).</li>
                  
                  <li class="text-left">Sebagai perusahaan yang mampu dan bisa berkembang sesuai kebutuhan.</li>
                  
                  <li class="text-left">Menjadi ruang untuk pengembangan usaha secara berkesinambungan.</li>
                  
                  <li class="text-left">Melakukan kegiatan bisnis dengan memperhatikan tata kelola perusahaan yang baik (good corporate government).</li>
                </ol>
              </div>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Pricing Table Section -->

    <!-- Counter Section Start -->
    <div class="counters section" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row"> 
          <div class="col-sm-6 col-md-4 col-lg-4">
            <div class="facts-item"> 
              <div class="icon">
                <i class="lnr lnr-user"></i>
              </div>              
              <div class="fact-count">
                <h3><span class="counter"><?php echo $klien ?></span></h3>
                <h4>Jumlah Klien</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4">
            <div class="facts-item">   
              <div class="icon">
                <i class="lnr lnr-clock"></i>
              </div>             
              <div class="fact-count">
                <h3><span class="text" style="font-size: 25px;">09.00 - 16.00 WIB</span></h3>
                <h4>Jam Kerja</h4>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4">
            <div class="facts-item">   
              <div class="icon">
                <i class="lnr lnr-briefcase"></i>
              </div>            
              <div class="fact-count">
                <h3><span class="counter"><?php echo $pemesanan ?></span></h3>
                <h4>Proyek Selesai</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  
    <!-- Counter Section End -->

    <!-- Portfolio Section -->
    <!-- Portfolio Section Ends --> 

    <!-- Blog Section -->
    <section id="blog" class="section">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Berita Perusahaan</h2>
          <hr class="lines">
        </div>
        <div class="row">
        <?php
		        foreach($data as $d){
		    ?>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 blog-item">
            <!-- Blog Item Starts -->
            <div class="blog-item-wrapper">
              <div class="blog-item-img">
                <a href="<?php echo base_url() ?>berita/detail/<?php echo $d->SLUG ?>">
                  <img style="max-height:278px;width:auto;max-width:100%" src="<?php echo base_url() ?>assets/images/berita/<?php echo $d->GAMBAR ?>" alt="">
                </a>                
              </div>
              <div class="blog-item-text"> 
                <div class="meta-tags">
                  <span class="date"><i class="lnr  lnr-calendar-full"></i> <?php echo date_format(date_create($d->CREATE_AT),"d M Y") ?></span>
                  <!-- <span class="comments"><a href="#"><i class="lnr lnr-bubble"></i> 24 Comments</a></span> -->
                </div>
                <h3>
                  <a href="<?php echo base_url() ?>berita/detail/<?php echo $d->SLUG ?>"><?php echo $d->JUDUL ?></a>
                </h3>
                <p>
                <?php echo substr(strip_tags($d->ISI), 0, 128) ?>...
                </p>
                <a href="berita/detail/<?php echo $d->SLUG ?>" class="btn-rm">Read More <i class="lnr lnr-arrow-right"></i></a>
              </div>
            </div>
            <!-- Blog Item Wrapper Ends-->
          </div>
        <?php } ?>
        </div>
        <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <div class="controls text-center">
              <?php 
                if($countberita<1) echo"Berita Kosong";
                else if($countberita>3) echo '<a class="btn btn-common" href="berita" data-filter="all">Tampilkan Lebih Banyak.... </a>';
              ?>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
          </div>
        </div>
      </div>
    </section>
    <!-- blog Section End -->

    <!-- Contact Section Start -->
    <section id="contact" class="section" data-stellar-background-ratio="-0.2">      
      <div class="contact-form">
        <div class="container">
          <div class="row">     
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <div class="contact-us">
                <h3>Hubungi Kami</h3>
                <div class="contact-address">
                  <p>Jl. Pulosari 3-J/17.B, Kel. Gunungsari, Kec. Dukuh Pakis, Surabaya - Jawa Timur</p>
                  <p class="phone">Phone: <span>(+62815616556)</span></p>
                  <p class="email">E-mail: <span>(bsetransportindo@gmail.com)</span></p>
                </div>
                <div class="social-icons">
                  <ul>
                    <li class="facebook"><a href="https://www.facebook.com/budi.transportindo"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="https://twitter.com/BTransportindo?lang=en"><i class="fa fa-twitter"></i></a></li>
                    <li class="google-plus"><a href="https://plus.google.com/u/2/109261065232857880997"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="https://www.instagram.com/bstransportindo/"><i class="fa fa-instagram"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>     
            <div class="col-lg-6 col-sm-6 col-xs-12">
            <?php echo $this->session->flashdata('notif') ?>
              <div class="contact-block">
                <form id="contactForm">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" class="form-control" id="name" name="nama" placeholder="Nama" required data-error="Masukan Nama Anda">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="email" placeholder="Email" id="email" class="form-control" name="email" required data-error="Masukan Email Anda">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <input type="text" pattern="[0-9]+" placeholder="No. Telp" id="telp" class="form-control" name="telp" required data-error="Masukan No. Telpon Anda">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group"> 
                        <textarea class="form-control" name="pertanyaan" id="message" placeholder="Pesan Anda" rows="6" data-error="Tuliskan Pesan Anda" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      <div class="submit-button text-center">
                        <button class="btn btn-common" id="submit" type="submit">Kirim</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>           
    </section>
    <!-- Contact Section End -->