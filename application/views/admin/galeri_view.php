<script src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    var loadFile = function(event) {
        var output = document.getElementById('mygambar');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Galeri</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <div class="row">
                        <div class="col-sm-6"><h4 class="mb"><i class="fa fa-angle-right"></i> Tabel Galeri</h4></div>
                        <div class="col-sm-6" style="text-align:right"><button data-toggle="modal" data-target="#myModalAdd" class="btn btn-success"><i class="fa fa-plus "></i> Tambah</button></div>
                    </div>
                    <table id="example" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Gambar</th>
                                <th>Deskripsi</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>                        
                        <tbody>
                        <?php 
		                    $no = 1;
		                    foreach($data as $d){ 
		                ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $d->JUDUL ?></td>
                                <td><img height="64px" src="<?php echo base_url() ?>assets/images/galeri/<?php echo $d->GAMBAR ?>" alt="No Image"></td>
                                <td><?php echo $d->DESKRIPSI ?></td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalDetail<?php echo $no ?>" data-id="<?php echo $d->ID_GALERI ?>" class="btn btn-warning"><i class="fa fa-eye"></i> Lihat</button>
                                    <div class="modal fade" id="myModalDetail<?php echo $no ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel"><?php echo $d->JUDUL ?></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <img style="max-width:100%;" src="<?php echo base_url() ?>assets/images/galeri/<?php echo $d->GAMBAR ?>" alt="No Image">
                                                </div>
                                                <div class="modal-footer" style="text-align:justify">
                                                    <?php echo $d->DESKRIPSI ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalEdit" data-id="<?php echo $d->ID_GALERI ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModal<?php echo $no ?>" class="btn btn-danger"><i class="fa fa-trash-o "></i> Hapus</button>
                                    <div class="modal fade" id="myModal<?php echo $no ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Hapus Foto</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus galeri <strong><?php echo $d->JUDUL ?></strong>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                    <form style="all: unset;" action="<?php echo base_url() ?>admin/galeri/hapus" method="post">
                                                        <input type="hidden" name="id" value="<?php echo $d->ID_GALERI ?>">
                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-edit">
        edit disini
    </div>
</div>

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Foto</h4>
            </div>
            <div class="modal-body">
                <div class="form-panel" style="box-shadow:none;">
                    <form class="form-horizontal style-form" method="post" action="<?php echo base_url(). 'admin/galeri/insert'; ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Judul</label>
                            <div class="col-sm-10">
                                <input id="judul" type="text" required name="judul" class="form-control" placeholder="Judul galeri">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label"></label>
                            <div class="col-sm-4">
					        	<div class="project-wrapper">
		                            <div class="project">
		                                <div class="">
		                                    <div class="photo">
		                                    	<a class="fancybox"><img class="img-responsive" id="mygambar" src="" alt="No Image"></a>
		                                    </div>
		                                    <div class="overlay"></div>
		                                </div>
		                            </div>
		                        </div>
					        </div><!-- col-lg-4 -->
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" required accept="image/*" onchange="loadFile(event)" style="height:auto" id="mytxtgambar" name="gambar" class="form-control" id="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Deskripsi</label>
                            <div class="col-sm-10">
                                <textarea name="deskripsi" class="form-control" cols="30" rows="3"></textarea>
                            </div>
                        </div>
                        <div style="text-align: right;">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
	   $('#example').DataTable({
        "scrollX": true
       });
	} );
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModalEdit').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'get',
                url : '<?php echo base_url() ?>admin/galeri/edit',
                data :  'id='+ rowid,
                success : function(data){
                    $('#fetched-data-edit').html(data);//menampilkan data ke dalam modal
                    validasi();
                }
            });
        });
    });
</script>