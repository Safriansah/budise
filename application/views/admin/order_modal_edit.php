<div class="modal-content">
    <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Data Pemesanan</h4>
    </div>
    <div class="modal-body">
        <div class="form-panel" style="box-shadow:none;">
            <?php foreach($data as $d) { ?>
            <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/order/update'; ?>" method="post">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Kode Pesan</label>
                    <div class="col-sm-10">
                        <input type="text" name="kode" value="<?php echo $d->KODE_PESAN ?>" readonly maxlength="10" required  class="form-control" placeholder="Kode Pemesanan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama Pemesan</label>
                    <div class="col-sm-10">
                        <input type="text" name="nama" required class="form-control" value="<?php echo $d->PEMESAN ?>" placeholder="Nama Pemesan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nomor Telpon Pemesan</label>
                    <div class="col-sm-10">
                        <input type="text" pattern="[0-9]+" value="<?php echo $d->TELP_PEMESAN ?>" name="telp" class="form-control" placeholder="Nomor Telpon Pemesan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Email Pemesan</label>
                    <div class="col-sm-10">
                        <input type="email" name="email"  class="form-control" value="<?php echo $d->EMAIL_PEMESAN ?>" placeholder="Email Pemesan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama Pengirim</label>
                    <div class="col-sm-10">
                        <input type="text" name="pengirim" required class="form-control" value="<?php echo $d->PENGIRIM ?>" placeholder="Perusahaan atau Pihak Terkait Pengirim">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Alamat Kirim</label>
                    <div class="col-sm-10">
                        <input type="text" name="alamat_kirim" required value="<?php echo $d->ALAMAT_KIRIM ?>" class="form-control" placeholder="Alamat Barang Diambil">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama Penerima</label>
                    <div class="col-sm-10">
                        <input type="text" name="penerima" required class="form-control" value="<?php echo $d->PENERIMA ?>" placeholder="Perusahaan atau Pihak Terkait Tujuan Pengiriman">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Alamat Tujuan</label>
                    <div class="col-sm-10">
                        <input type="text" name="alamat_penerima" value="<?php echo $d->ALAMAT_PENERIMA ?>" required class="form-control" placeholder="Alamat Tujuan Pengiriman">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Tanggal Kirim</label>
                    <div class="col-sm-10">
                        <input style="height:auto" type="date" name="tgl_kirim" value="<?php echo $d->TGL_KIRIM ?>" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Total Harga</label>
                    <div class="col-sm-10">
                        <input type="number" name="harga" required class="form-control" value="<?php echo $d->TOTAL_HARGA ?>" placeholder="Total Harga dalam satuan Rupiah"> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="status">
                            <option value="1" <?php echo $d->STATUS=="1"?"selected":" " ?>>Belum Dikirim</option>
                            <option value="2" <?php echo $d->STATUS=="2"?"selected":" " ?>>Proses</option>
                            <option value="3" <?php echo $d->STATUS=="3"?"selected":" " ?>>Sudah Terkirim</option>
                        </select> 
                    </div>
                </div>
                <div class="row" style="text-align: right;">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </form>
            <?php } ?>
        </div>
    </div>
</div>    