<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    
    <link rel="shortcut icon" type="image/jpg" href="<?php echo base_url() ?>assets/images/fav.jpg"/>
    <title>Admin - CV. Budi Setiawan Enterprise</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/lineicons/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/DataTables/DataTables-1.10.16/css/jquery.dataTables.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/css/style-responsive.css" rel="stylesheet">
    <script src="<?php echo base_url() ?>assets/js/chart-master/Chart.js"></script>
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg" style="">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="<?php echo base_url() ?>admin" class="logo"><b><img style="height:24px"src="<?php echo base_url() ?>assets/images/logo.png" alt=""></b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu" >
                    <li><a class="logout" style="color:#fff" href="<?php echo base_url() ?>admin/logout">Keluar</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->

      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">

              	  <p class="centered"><a style="color:#ccc" href="<?php echo base_url() ?>admin/profile"><img src="<?php echo base_url() ?>assets/images/ava.jpg" class="img-circle" width="60">
              	  <h5 class="centered"><?php echo $this->session->userdata('user') ?></h5></a></p>

                  <li class="mt">
                      <a class="<?php echo $menu=='0'?'active':"" ?>" href="<?php echo base_url() ?>admin">
                          <i class="fa fa-dashboard"></i>
                          <span>Beranda</span>
                      </a>
                  </li>
                
                  <?php if($this->session->userdata('role')<2){ ?>
                  <li class="sub-menu">
                      <a class="<?php echo $menu=='1'?'active':"" ?>" href="<?php echo base_url() ?>admin/pegawai">
                          <i class="fa fa-black-tie"></i>
                          <span>Pegawai</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php echo $menu=='2'?'active':"" ?>" href="<?php echo base_url() ?>admin/user">
                          <i class="fa fa-user-circle-o"></i>
                          <span>User</span>
                      </a>
                  </li>
                  <?php } ?>
                    
                  <?php if($this->session->userdata('role')<3){ ?>
                  <li class="sub-menu">
                      <a class="<?php echo $menu=='3'?'active':"" ?>" href="<?php echo base_url() ?>admin/order">
                          <i class="fa fa-file-text-o"></i>
                          <span>Pesanan</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php echo $menu=='4'?'active':"" ?>" href="<?php echo base_url() ?>admin/pertanyaan">
                          <i class="fa fa-question-circle-o"></i>
                          <span>Pertanyaan</span>
                      </a>
                  </li>
                  <?php } ?>

                  <li class="sub-menu">
                      <a class="<?php echo $menu=='5'?'active':"" ?>" href="javascript:;">
                          <i class="fa fa-newspaper-o"></i>
                          <span>Berita</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="<?php echo base_url() ?>admin/berita/tambah"><i class="fa fa-plus" aria-hidden="true"></i> Tambah Berita</a></li>
                          <li><a  href="<?php echo base_url() ?>admin/berita"><i class="fa fa-list-ul" aria-hidden="true"></i> Tabel Berita</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php echo $menu=='6'?'active':"" ?>" href="<?php echo base_url() ?>admin/galeri">
                          <i class="fa fa-picture-o"></i>
                          <span>Galeri</span>
                      </a>
                  </li>
                  </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
        
        <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url() ?>assets/js/jquery-1.8.3.min.js"></script>