<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Data Pegawai</h4>
    </div>
    <div class="modal-body">
        <div class="form-panel" style="box-shadow:none;">
        <?php foreach($data as $d){ ?>
            <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/pegawai/update'; ?>" method="post">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">NIP</label>
                    <div class="col-sm-10">
                        <input type="text" maxlength="10" readonly value="<?php echo $d->NIP ?>" name="nip" required  class="form-control" placeholder="NIP Pegawai">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" name="nama" value="<?php echo $d->NAMA ?>" required class="form-control" placeholder="Nama Lengkap Pegawai">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="gender">
                            <option value="1" <?php echo $d->GENDER=='1'?"selected":"" ?>>Laki-laki</option>
                            <option value="2" <?php echo $d->GENDER=='2'?"selected":"" ?>>Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Tanggal Lahir</label>
                    <div class="col-sm-10">
                        <input type="date" name="lahir" style="height:auto" value="<?php echo $d->TGL_LAHIR ?>" required class="form-control" placeholder="Date Picker">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea name="alamat" required class="form-control" id="" cols="30" rows="3"><?php echo $d->ALAMAT ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nomor Telepon</label>
                    <div class="col-sm-10">
                        <input type="text" pattern="[0-9]+" maxlength="16" name="telp" required class="form-control" value="<?php echo $d->TELP ?>" placeholder="085000000000">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" required class="form-control" value="<?php echo $d->EMAIL ?>" placeholder="xxx@gmail.com">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Jabatan</label>
                    <div class="col-sm-10">
                        <input type="text" value="<?php echo $d->JABATAN ?>" name="jabatan" required class="form-control" placeholder="Jabatan Pegawai">
                    </div>
                </div>
                <div class="row" style="text-align: right;">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </form>
        <?php } ?>
        </div>
    </div>
</div>