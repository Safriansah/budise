<script src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector:"textarea",
        height:"256"
    });
    var loadFile = function(event) {
        var output = document.getElementById('mygambar');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Berita</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Tambah Berita</h4>
                    <form class="form-horizontal style-form" method="post" action="<?php echo base_url(). 'admin/berita/insert'; ?>" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Judul</label>
                            <div class="col-sm-10">
                                <input id="judul" type="text" required name="judul" class="form-control" placeholder="Judul Berita">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Isi Berita</label>
                            <div class="col-sm-10">
                                <textarea name="isi" class="form-control" id="isi" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label"></label>
                            <div class="col-sm-4">
					        	<div class="project-wrapper">
		                            <div class="project">
		                                <div class="">
		                                    <div class="photo">
		                                    	<a class="fancybox"><img class="img-responsive" id="mygambar" src="" alt="No Image"></a>
		                                    </div>
		                                    <div class="overlay"></div>
		                                </div>
		                            </div>
		                        </div>
					        </div><!-- col-lg-4 -->
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" accept="image/*" onchange="loadFile(event)" style="height:auto" id="mytxtgambar" name="gambar" class="form-control" id="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">URL</label>
                            <div class="col-sm-10">
                                <?php echo base_url() ?>berita/detail/
                                <input id="url" required type="text" name="url" class="form-control" placeholder="URL Berita">
                                <button id="generate" pattern="[^()/<>[\]\\,'|\x22]+" type="button" class="btn btn-success" style="margin-top:4px"><i class="fa fa-refresh"></i> Generate</button>
                            </div>
                        </div>
                        <div style="text-align: right;">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>
<script>
    $("#generate").click(function(){
        var judul=$("#judul").val();
        var url = judul.replace(/\s+/g, '-').toLowerCase();
        url = url.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        $("#url").val(url);
    });
</script>
