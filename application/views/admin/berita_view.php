<script src="<?php echo base_url() ?>assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    var loadFile = function(event) {
        var output = document.getElementById('mygambar');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Berita</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Tabel Berita</h4>
                    <table id="example" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Isi Berita</th>
                                <th>Gambar</th>
                                <th>Tampil</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>                        
                        <tbody>
                        <?php 
		                    $no = 1;
		                    foreach($data as $d){ 
		                ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $d->JUDUL ?></td>
                                <td><?php echo substr(strip_tags($d->ISI), 0, 96) ?>...</td>
                                <td><img height="64px" src="<?php echo base_url() ?>assets/images/berita/<?php echo $d->GAMBAR ?>" alt="No Image"></td>
                                <td><?php echo $d->TAMPIL ?></td>
                                <td style="text-align:center">
                                    <a href="<?php echo base_url() ?>berita/detail/<?php echo $d->SLUG ?>">
                                        <button data-toggle="modal" data-target="#myModalDetail" data-id="<?php echo $d->ID_BERITA ?>" class="btn btn-warning"><i class="fa fa-eye"></i> Lihat</button>
                                    </a>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalEdit" data-id="<?php echo $d->ID_BERITA ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModal<?php echo $no ?>" class="btn btn-danger"><i class="fa fa-trash-o "></i> Hapus</button>
                                    <div class="modal fade" id="myModal<?php echo $no ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Hapus Berita</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus berita <strong><?php echo $d->JUDUL ?></strong>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                    <form style="all: unset;" action="<?php echo base_url() ?>admin/berita/hapus" method="post">
                                                        <input type="hidden" name="id" value="<?php echo $d->ID_BERITA ?>">
                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>

<div class="modal fade" id="myModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-detail" style="width:85%;margin:30px auto;">
        detail disini
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-edit" style="width:85%;margin:30px auto;">
        edit disini
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
	   $('#example').DataTable({
        "scrollX": true
       });
	} );
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModalDetail').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'get',
                url : '<?php echo base_url() ?>admin/berita/detail',
                data :  'id='+ rowid,
                success : function(data){
                    $('#fetched-data-detail').html(data);//menampilkan data ke dalam modal
                    validasi();
                }
            });
        });
        
        $('#myModalEdit').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'get',
                url : '<?php echo base_url() ?>admin/berita/edit',
                data :  'id='+ rowid,
                success : function(data){
                    $('#fetched-data-edit').html(data);//menampilkan data ke dalam modal
                    validasi();
                }
            });
        });
    });
</script>