<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Pertanyaan</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <div class="row">
                        <div class="col-sm-6"><h4 class="mb"><i class="fa fa-angle-right"></i> Tabel Pertanyaan</h4></div>
                        <div class="col-sm-6" style="text-align:right"><button data-toggle="modal" data-target="#myModalAdd" class="btn btn-success"><i class="fa fa-plus "></i> Tambah</button></div>
                    </div>
                    <table id="example" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telpon</th>
                                <th>Pertanyaan</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>                        
                        <tbody>
                        <?php 
		                    $no = 1;
		                    foreach($data as $d){ 
		                ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $d->NAMA ?></td>
                                <td><?php echo $d->EMAIL ?></td>
                                <td><?php echo $d->TELP ?></td>
                                <td><?php echo $d->PERTANYAAN ?></td>
                                <td><?php echo $d->STATUS=="0"?"Belum Dijawab":($d->STATUS=="1"?"Sudah Dijawab":"Error") ?></td>
                                <td><?php echo $d->CREATE_AT ?></td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalEdit" data-id="<?php echo $d->ID_PERTANYAAN ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModal<?php echo $no ?>" class="btn btn-danger"><i class="fa fa-trash-o "></i> Hapus</button>
                                    <div class="modal fade" id="myModal<?php echo $no ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Hapus Data Pertanyaan</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus data <strong><?php echo $d->NAMA ?> ( <?php echo $d->PERTANYAAN ?> )</strong>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                    <form style="all: unset;" action="<?php echo base_url() ?>admin/pertanyaan/hapus" method="post">
                                                        <input type="hidden" name="id" value="<?php echo $d->ID_PERTANYAAN ?>">
                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-add">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Tambah Pertanyaan</h4>
            </div>
            <div class="modal-body">
                <div class="form-panel" style="box-shadow:none;">
                    <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/pertanyaan/insert'; ?>" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" required  class="form-control" placeholder="Nama Customer Yang Bertanya">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" required class="form-control" placeholder="Email Yang Akan Dihubungi Untuk Diberikan Jawaban">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nomor Telpon</label>
                            <div class="col-sm-10">
                                <input type="text" pattern="[0-9]+" name="telp" required class="form-control" placeholder="Nomor Yang Akan Dihubungi Untuk Diberikan Jawaban">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Pertanyaan</label>
                            <div class="col-sm-10">
                                <textarea name="pertanyaan" id="" cols="30" class="form-control" rows="10" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Status</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="status">
                                    <option value="0">Belum Dijawab</option>
                                    <option value="1">Sudah Dijawab</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" style="text-align: right;">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>   
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-edit">
        detail disini
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
	   $('#example').DataTable({
        "scrollX": true
       });
	} );
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('#myModalEdit').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        //menggunakan fungsi ajax untuk pengambilan data
        $.ajax({
            type : 'get',
            url : '<?php echo base_url() ?>admin/pertanyaan/edit',
            data :  'id='+ rowid,
            success : function(data){
                $('#fetched-data-edit').html(data);//menampilkan data ke dalam modal
                validasi();
            }
        });
    });
});
</script>