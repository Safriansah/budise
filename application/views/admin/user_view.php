<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Pegawai</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <div class="row">
                        <div class="col-sm-6"><h4 class="mb"><i class="fa fa-angle-right"></i> Tabel User</h4></div>
                    </div>
                    <table id="example" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>                        
                        <tbody>
                        <?php 
		                    $no = 1;
		                    foreach($data as $d){ 
		                ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $d->NIP ?></td>
                                <td><?php echo $d->NAMA ?></td>
                                <td><?php echo $d->USERNAME ?></td>
                                <td><?php echo $d->ROLE=='1'?"SuperAdmin":($d->ROLE=='2'?"Admin":($d->ROLE=='3'?"Author":"error")) ?></td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalEdit" data-id="<?php echo $d->ID_USER ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                </td>
                                <td style="text-align:center">
                                    <?php if($d->ROLE!=1){?>
                                    <button data-toggle="modal" data-target="#myModal<?php echo $no ?>" class="btn btn-danger"><i class="fa fa-trash-o "></i> Hapus</button>
                                    <div class="modal fade" id="myModal<?php echo $no ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Hapus User</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus data <strong><?php echo $d->USERNAME ?> ( <?php echo $d->NAMA ?> )</strong>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                    <form style="all: unset;" action="<?php echo base_url() ?>admin/user/hapus" method="post">
                                                        <input type="hidden" name="id" value="<?php echo $d->ID_USER ?>">
                                                        <input type="hidden" name="idp" value="<?php echo $d->ID_PGW ?>">
                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?> 
                                </td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>
<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-edit">
        edit disini
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
	   $('#example').DataTable({
        "scrollX": true
       });
	} );
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#myModalEdit').on('show.bs.modal', function (e) {
            var rowid = $(e.relatedTarget).data('id');
            //menggunakan fungsi ajax untuk pengambilan data
            $.ajax({
                type : 'get',
                url : '<?php echo base_url() ?>admin/user/edit',
                data :  'id='+ rowid,
                success : function(data){
                    $('#fetched-data-edit').html(data);//menampilkan data ke dalam modal
                    validasi();
                }
            });
         });
    });
</script>