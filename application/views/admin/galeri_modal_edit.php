<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Foto</h4>
    </div>    
    <div class="modal-body">
        <div class="form-panel" style="box-shadow:none;">
        <?php foreach($data as $d){ ?>
            <form class="form-horizontal style-form" method="post" action="<?php echo base_url(). 'admin/galeri/update'; ?>" enctype="multipart/form-data">
            <input type="hidden" value="<?php echo $d->ID_GALERI ?>" name="id" >
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Judul</label>
                    <div class="col-sm-10">
                        <input id="judul" value="<?php echo $d->JUDUL ?>" type="text" required name="judul" class="form-control" placeholder="Judul galeri">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-4">
			        	<div class="project-wrapper">
		                    <div class="project">
		                        <div class="">
		                            <div class="photo">
		                            	<a class="fancybox"><img class="img-responsive" src="<?php echo base_url() ?>assets/images/galeri/<?php echo $d->GAMBAR ?>" id="mygambar" src="" alt="No Image"></a>
		                            </div>
		                            <div class="overlay"></div>
		                        </div>
		                    </div>
		                </div>
			        </div><!-- col-lg-4 -->
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Gambar</label>
                    <div class="col-sm-10">
                        <input type="file" accept="image/*" onchange="loadFile(event)" style="height:auto" id="mytxtgambar" name="gambar" class="form-control" id="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Deskripsi</label>
                    <div class="col-sm-10">
                        <textarea name="deskripsi" class="form-control" cols="30" rows="3"><?php echo $d->DESKRIPSI ?></textarea>
                    </div>
                </div>
                <div style="text-align: right;">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </form>
        <?php } ?>
        </div>
    </div>
</div>