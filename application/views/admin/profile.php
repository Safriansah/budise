<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Profile</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Data Pegawai</h4>
                    <?php foreach($data as $d){ ?>
                    <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/profile/update'; ?>" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">NIP</label>
                            <div class="col-sm-10">
                                <input type="text" readonly maxlength="10" value="<?php echo $d->NIP ?>" name="nip" required  class="form-control" placeholder="NIP Pegawai">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" value="<?php echo $d->NAMA ?>" required class="form-control" placeholder="Nama Lengkap Pegawai">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Jenis Kelamin</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="gender">
                                    <option value="1" <?php echo $d->GENDER=='1'?"selected":"" ?>>Laki-laki</option>
                                    <option value="2" <?php echo $d->GENDER=='2'?"selected":"" ?>>Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Tanggal Lahir</label>
                            <div class="col-sm-10">
                                <input type="date" name="lahir" style="height:auto" value="<?php echo $d->TGL_LAHIR ?>" required class="form-control" placeholder="Date Picker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <textarea name="alamat" required class="form-control" id="" cols="30" rows="3"><?php echo $d->ALAMAT ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Nomor Telepon</label>
                            <div class="col-sm-10">
                                <input type="text" pattern="[0-9]+" maxlength="16" name="telp" required class="form-control" value="<?php echo $d->TELP ?>" placeholder="085000000000">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" required class="form-control" value="<?php echo $d->EMAIL ?>" placeholder="xxx@gmail.com">
                            </div>
                        </div>
                        <div style="text-align: right;">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                        </div>
                    </form>
                    <?php } ?>
                </div>

                <div class="form-panel">
                    <h4 class="mb"><i class="fa fa-angle-right"></i> Data User</h4>
                    <?php foreach($user as $d){ ?>
                    <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/profile/update_user'; ?>" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="hidden" value="<?php echo $d->ID_PGW ?>" name="id" required>
                                <input type="text" value="<?php echo $d->USERNAME ?>" name="username" required  class="form-control" placeholder="Username untuk login pada sistem">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control" placeholder="Kosongkan jika tidak diganti">
                            </div>
                        </div>
                        <div style="text-align: right;">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                        </div>
                    </form>
                    <?php } ?>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>
