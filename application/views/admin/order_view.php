<section id="main-content">
    <section class="wrapper site-min-height">
        <h3><i class="fa fa-angle-right"></i> Pemesanan</h3>
        <div class="row mt">
            <div class="col-lg-12">
                <?php echo $this->session->flashdata('notif') ?>
                <div class="form-panel">
                    <div class="row">
                        <div class="col-sm-6"><h4 class="mb"><i class="fa fa-angle-right"></i> Tabel Pemesanan</h4></div>
                        <div class="col-sm-6" style="text-align:right"><button data-toggle="modal" data-target="#myModalAdd" class="btn btn-success"><i class="fa fa-plus "></i> Tambah</button></div>
                    </div>
                    <table id="example" class="table table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode Pesan</th>
                                <th>Nama Pemesan</th>
                                <th>Dikirm Dari</th>
                                <th>Dikirim Ke</th>
                                <th>Tanggal Kirim</th>
                                <th>Total Harga</th>
                                <th>Status</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>                        
                        <tbody>
                        <?php 
		                    $no = 1;
		                    foreach($data as $d){ 
		                ?>
                            <tr>
                                <td><?php echo $no ?></td>
                                <td><?php echo $d->KODE_PESAN ?></td>
                                <td><?php echo $d->PEMESAN ?></td>
                                <td><?php echo $d->ALAMAT_KIRIM ?></td>
                                <td><?php echo $d->ALAMAT_PENERIMA ?></td>
                                <td><?php echo $d->TGL_KIRIM ?></td>
                                <td><?php echo $d->TOTAL_HARGA ?></td>
                                <td><?php echo $d->STATUS=='1'?"Belum Dikirim":($d->STATUS=='2'?"Proses":($d->STATUS=='3'?"Sudah Terkirim":"error")) ?></td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalDetail" data-id="<?php echo $d->ID_PESAN ?>" class="btn btn-warning"><i class="fa fa-eye"></i> Lihat</button>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModalEdit" data-id="<?php echo $d->ID_PESAN ?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Edit</button>
                                </td>
                                <td style="text-align:center">
                                    <button data-toggle="modal" data-target="#myModal<?php echo $no ?>" class="btn btn-danger"><i class="fa fa-trash-o "></i> Hapus</button>
                                    <div class="modal fade" id="myModal<?php echo $no ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">Hapus Data Pemesanan</h4>
                                                </div>
                                                <div class="modal-body">
                                                    Anda yakin ingin menghapus data <strong><?php echo $d->PEMESAN ?> ( <?php echo $d->KODE_PESAN ?> )</strong>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                    <form style="all: unset;" action="<?php echo base_url() ?>admin/order/hapus" method="post">
                                                        <input type="hidden" name="id" value="<?php echo $d->ID_PESAN ?>">
                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php $no++; } ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- col-lg-12-->      	
        </div><!-- /row -->
    </section>
</section>

<div class="modal fade" id="myModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-add">
        add disini    
    </div>
</div>

<div class="modal fade" id="myModalDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-detail">
        detail disini
    </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="fetched-data-edit">
        detail disini
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>assets/DataTables/DataTables-1.10.16/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
	   $('#example').DataTable({
        "scrollX": true
       });
	} );
</script>

<script type="text/javascript">
$(document).ready(function(){
    $('#myModalAdd').on('show.bs.modal', function (e) {
        //menggunakan fungsi ajax untuk pengambilan data
        $.ajax({
            type : 'get',
            url : '<?php echo base_url() ?>admin/order/create',
            success : function(data){
                $('#fetched-data-add').html(data);//menampilkan data ke dalam modal
                validasi();
            }
        });
    });
    
    $('#myModalDetail').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        //menggunakan fungsi ajax untuk pengambilan data
        $.ajax({
            type : 'get',
            url : '<?php echo base_url() ?>admin/order/detail',
            data :  'id='+ rowid,
            success : function(data){
                $('#fetched-data-detail').html(data);//menampilkan data ke dalam modal
                validasi();
            }
        });
    });
    
    $('#myModalEdit').on('show.bs.modal', function (e) {
        var rowid = $(e.relatedTarget).data('id');
        //menggunakan fungsi ajax untuk pengambilan data
        $.ajax({
            type : 'get',
            url : '<?php echo base_url() ?>admin/order/edit',
            data :  'id='+ rowid,
            success : function(data){
                $('#fetched-data-edit').html(data);//menampilkan data ke dalam modal
                validasi();
            }
        });
    });
});
</script>