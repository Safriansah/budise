<section id="main-content">
        <section class="wrapper site-min-height">
        	<h3><i class="fa fa-angle-right"></i> Selamat Datang di Halaman Admin</h3>
        	<div class="row mtbox">
                <div class="col-md-3 col-sm-3 col-md-offset-1 box0" style="border-radius:24px;text-align: center;">
			<h4>Pegawai</h4>
			<div class="box1">
				<span class="li_user"></span>
				<h3><?php echo $pgw ?></h3>
                	</div>
			<p><?php echo $pgw ?> Pegawai Terdaftar.</p>
                </div>
                <div class="col-md-3 col-sm-3 box0" style="border-radius:24px;text-align: center;">
			<h4>Berita</h4>
                	<div class="box1">
				<span class="li_news "></span>
				<h3><?php echo $berita ?></h3>
                	</div>
			<p><?php echo $berita ?> Berita Telah Dipublish.</p>
                </div>
                <div class="col-md-3 col-sm-3 box0" style="border-radius:24px;text-align: center;">
			<h4>Pemesanan</h4>
			<div class="box1">
				<span class="li_truck "></span>
				<h3><?php echo $pesan ?></h3>
                	</div>
			<p><?php echo $pesan ?> Data Pemesanan.</p>
                </div>
		<div class="col-md-3 col-sm-3 col-md-offset-1 box0" style="border-radius:24px;text-align: center;">
			<h4>Pertanyaan</h4>
			<div class="box1">
				<span class="li_bubble"></span>
				<h3><?php echo $tanya ?></h3>
                	</div>
			<p><?php echo $tanya ?> Pertanyaan Masuk.</p>
                </div>
		<div class="col-md-3 col-sm-3 box0" style="border-radius:24px;text-align: center;">
			<h4>Galeri</h4>
			<div class="box1">
				<span class="li_photo"></span>
				<h3><?php echo $galeri ?></h3>
                	</div>
			<p><?php echo $galeri ?> Foto Dalam Galeri.</p>
                </div>
        	</div><!-- /row mt -->
	</section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->