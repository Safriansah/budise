<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Data Pemesanan</h4>
    </div>
    <div class="modal-body">
    <?php foreach($data as $d){ ?>
    <div class="form-horizontal style-form">
        <div class="form-panel" style="box-shadow:none;">
            <div class="form-group">
                <label class="col-sm-4 control-label">Kode Pesan</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->KODE_PESAN ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Nama Pemesan</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->PEMESAN ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Nomor Telpon Pemesan</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->TELP_PEMESAN ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Email Pemesan</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->EMAIL_PEMESAN ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Nama Pengirim</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->PENGIRIM ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Alamat Kirim</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->ALAMAT_KIRIM ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Nama Penerima</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->PENERIMA ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Alamat Tujuan</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->ALAMAT_PENERIMA ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Tanggal Kirim</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->TGL_KIRIM ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Total Harga</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : RP. <?php echo $d->TOTAL_HARGA ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->STATUS=='1'?"Belum Dikirim":($d->STATUS=='2'?"Proses":($d->STATUS=='3'?"Sudah Terkirim":"error")) ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Create At</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->CREATE_AT ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Create By</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $crby ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Update At</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $d->UPDATE_AT ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Update By</label>
                <div class="col-sm-8 control-label" style="font-weight:bold">
                    : <?php echo $upby ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    </div>
</div>