<div class="modal-content">
<?php foreach($data as $d) { ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Data Pertanyaan</h4>
        <h6><?php echo $d->STATUS=='0'?' ':'Pertanyaan ini telah dijawab oleh '.$crby ?></h6>
    </div>
    <div class="modal-body">
        <div class="form-panel" style="box-shadow:none;">
            <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/pertanyaan/update'; ?>" method="post">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="hidden" value="<?php echo $d->ID_PERTANYAAN ?>" name="id">
                        <input type="text" name="nama" required value="<?php echo $d->NAMA ?>"  class="form-control" placeholder="Nama Customer Yang Bertanya">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" name="email" required value="<?php echo $d->EMAIL ?>" class="form-control" placeholder="Email Yang Akan Dihubungi Untuk Diberikan Jawaban">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Nomor Telpon</label>
                    <div class="col-sm-10">
                        <input type="text" pattern="[0-9]+" name="telp" value="<?php echo $d->TELP ?>" required class="form-control" placeholder="Nomor Yang Akan Dihubungi Untuk Diberikan Jawaban">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Pertanyaan</label>
                    <div class="col-sm-10">
                        <textarea name="pertanyaan" id="" cols="30" class="form-control" rows="10" required><?php echo $d->PERTANYAAN ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="status">
                            <option value="0" <?php echo $d->STATUS=='0'?'selected':''?>>Belum Dijawab</option>
                            <option value="1" <?php echo $d->STATUS=='1'?'selected':''?>>Sudah Dijawab</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="text-align: right;">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
<?php } ?>
</div>    