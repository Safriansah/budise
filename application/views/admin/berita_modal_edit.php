<script type="text/javascript">
    tinymce.remove();
    tinymce.init({
        selector:"textarea",
        height:"256"
    });
</script>
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Berita</h4>
    </div>    
    <div class="modal-body">
        <div class="form-panel" style="box-shadow:none;">
        <?php foreach($data as $d){ ?>
            <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/berita/update'; ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Judul</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="id" value="<?php echo $d->ID_BERITA ?>">
                        <input id="judul" type="text" value="<?php echo $d->JUDUL ?>" required name="judul" class="form-control" placeholder="Judul Berita">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Isi Berita</label>
                    <div class="col-sm-10">
                        <textarea name="isi" class="form-control" id="isi" cols="300" rows="20"><?php echo $d->ISI ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"></label>
                    <div class="col-sm-4">
				    	<div class="project-wrapper">
		                    <div class="project">
		                        <div class="">
		                            <div class="photo">
		                            	<a class="fancybox"><img class="img-responsive" id="mygambar" src="<?php echo base_url() ?>assets/images/berita/<?php echo $d->GAMBAR ?>" alt="No Image"></a>
		                            </div>
		                            <div class="overlay"></div>
		                        </div>
		                    </div>
		                </div>
				    </div><!-- col-lg-4 -->
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Gambar</label>
                    <div class="col-sm-10">
                        <input type="file" accept="image/*" name="gambar" onchange="loadFile(event)" style="height:auto" class="form-control" id="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">URL</label>
                    <div class="col-sm-10">
                        <?php echo base_url() ?>berita/detail/
                        <input id="url" type="text" name="url" class="form-control" placeholder="URL Berita" value="<?php echo $d->SLUG ?>">
                        <input type="hidden" name="old" value="<?php echo $d->SLUG ?>">
                        <button id="generate" pattern="[^()/<>[\]\\,'|\x22]+" type="button" class="btn btn-success" style="margin-top:4px"><i class="fa fa-refresh"></i> Generate</button>
                    </div>
                </div>
                <div class="row" style="text-align: right;">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </form>
        <?php } ?>
        </div>
    </div>
</div>
<script>
    $("#generate").click(function(){
        var judul=$("#judul").val();
        var url = judul.replace(/\s+/g, '-').toLowerCase();
        url = url.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
        $("#url").val(url);
    });
</script>
