<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Data Berita</h4>
    </div>
    <div class="modal-body">
    <div class="form-horizontal style-form">
    <?php foreach($data as $d){ ?>
    
        <div class="form-panel" style="box-shadow:none;">
            <div class="form-group">
                <label class="col-sm-2 control-label">Judul</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $d->JUDUL ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Isi</label>
                <div class="col-sm-10 control-label">
                    : <?php echo strip_tags($d->ISI,"<span><img><strong><code><p>") ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Gambar</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $d->GAMBAR ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Dibaca</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo base_url().$d->SLUG ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Dibaca</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $d->TAMPIL ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Create At</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $d->CREATE_AT ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Create By</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $crby ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Update At</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $d->UPDATE_AT ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Update By</label>
                <div class="col-sm-10 control-label" style="font-weight:bold">
                    : <?php echo $upby ?>
                </div>
            </div>
        </div>
    
    <?php } ?>
    </div>
    </div>
</div>