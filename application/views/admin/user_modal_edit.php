<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Data User</h4>
    </div>
    <div class="modal-body">
        <div class="form-panel" style="box-shadow:none;">
        <?php foreach($data as $d){ ?>
            <form class="form-horizontal style-form" action="<?php echo base_url(). 'admin/user/update'; ?>" method="post">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <input type="hidden" name="id" value="<?php echo $d->ID_USER ?>" required>
                        <input type="hidden" name="userold" value="<?php echo $d->USERNAME ?>" required>
                        <input type="text" name="username" value="<?php echo $d->USERNAME ?>" required class="form-control" placeholder="Username Pegawai">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" name="password" value="" class="form-control" placeholder="Kosongkan jika tidak diganti">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Role</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="tipe">
                            <option value="1" <?php echo $d->ROLE=='1'?'selected':"" ?>>SuperAdmin</option>
                            <option value="2" <?php echo $d->ROLE=='2'?'selected':"" ?>>Admin</option>
                            <!--<option value="3" <?php //echo $d->ROLE=='3'?'selected':"" ?>>Author</option>-->
                        </select>
                    </div>
                </div>
                <div class="row" style="text-align: right;">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </div>
            </form>
        <?php } ?>
        </div>
    </div>
</div>