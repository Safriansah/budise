  <body>
        <header style="height: 100px;" id="hero-area" data-stellar-background-ratio="0.5">    
                <!-- Navbar Start -->
                <nav class="navbar navbar-expand-lg scrolling-navbar fixed-top indigo">
                  <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <a href="<?php echo base_url() ?>" class="navbar-brand"><img class="img-fulid" src="<?php echo base_url() ?>assets/images/logo.png" height="40px" alt=""></a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="lnr lnr-menu"></i>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar">
                      <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>berita">Berita</a>
                        </li>
                      </ul>
                    </div>
                  </div>
          
                  <!-- Mobile Menu Start -->
                  <ul class="mobile-menu">
                     <li>
                        <a class="page-scroll" href="<?php echo base_url() ?>">Home</a>
                      </li>
                      <li>
                        <a class="nav-link page-scroll" href="<?php echo base_url() ?>berita">Berita</a>
                      </li>
                  </ul>
                  <!-- Mobile Menu End -->
          
                </nav>
                <!-- Navbar End -->           
              </header>

    <section id="portfolios" class="section" style="min-height: 450px;">
      <!-- Container Starts -->
      <div class="container">
        <div class="section-header">          
          <h2 class="section-title">Galeri Perusahaan</h2>
          <hr class="lines">
        </div>
        <div class="row"> 
          <!-- Portfolio Recent Projects -->
          <div id="portfolio" class="row">
          <?php
            $a=0;
		        foreach($data as $d){
              $a++;
		      ?>
            <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4 mix development print">
              <div class="portfolio-item">
                <div class="shot-item">
                  <img src="<?php echo base_url() ?>assets/images/galeri/<?php echo $d->GAMBAR ?>" alt="" />  
                  <a class="overlay lightbox" href="<?php echo base_url() ?>assets/images/galeri/<?php echo $d->GAMBAR ?>">
                    <i class="lnr lnr-eye item-icon"></i>
                  </a>
                </div>               
              </div>
            </div>
          <?php } ?>
          </div>
        </div>
        <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <div class="controls text-center">
              <?php 
                if($a<1) echo"Galeri Kosong";
              ?>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
          </div>
        </div>
      </div>
      <!-- Container Ends -->
    </section>
    <!-- Portfolio Section Ends --> 
    <section class="section">  
      <div class="container">
        <div class="row">
          <?php echo $paging ?>
        </div>
      </div>
    </section>