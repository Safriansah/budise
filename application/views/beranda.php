<!-- /.navbar -->

	<!-- Header -->
	<header id="head">
		<div class="container">
			<div class="row">
				<h1 class="lead">BSE Transportindo</h1>
				<p class="tagline">Starting a business is a huge amount of hard work, You had better enjoy it!</p>
			</div>
		</div>
	</header>
	<!-- /Header -->

	<!-- Intro -->
	<div class="container text-center">
		<br> <br>

		<h2 class="thin">Motto Perusahaan Kami?</h2>
		<div class="row">
			<div class="col-md-4 col-sm-6">
				<h2>Intergritas</h2>
				<div class="h-body text-center">
					<p>Setiap insan di perusahaan harus melakukan pekerjaannya dengan penuh tanggung jawab, professional dan penuh dedikasi.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<h2>Jujur</h2>
				<div class="h-body text-center">
					<p>Setiap insan ditanamkan nilai-nilai kejujuran dalam menjalankan amanah dari perusahaan untuk memberikan pelayanan yang terbaik bagi seluruh costumer.</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<h2>Peduli</h2>
				<div class="h-body text-center">
					<p>Setiap insan di perusahaan bertanggung jawab penuh untuk melayani, memperhatikan dan membantu costumer sesuai dengan kebutuhannya.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- /Intro-->
		
	<!-- Highlights - jumbotron -->
	<div class="jumbotron top-space">
		<div class="container">
			
			<h3 class="text-center thin" style="font-weight: bold">Apa Keunggulan Perusahaan Kami?</h3>
			
			<div class="row">
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-money fa-5"></i>Harga Bersahabat</h4></div>
					<div class="h-body text-center">
						<p>Budi Setiawan Enterprise Transportindo menawarkan harga yang lebih murah daripada jasa jasa logistic lainnya </p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-truck fa-5"></i>Pengiriman Cepat</h4></div>
					<div class="h-body text-center">
						<p>Budi Setiawan Enterprise Transportindo menjanjikan proses pengiriman yang tepat waktu jadi anda tidak perlu khawatir akan keterlambatan pengiriman </p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-lock fa-5"></i>Barang Dijamin Aman</h4></div>
					<div class="h-body text-center">
						<p>Budi Setiawan Enterprise Transportindo menjamin keamanan barang anda karena kurir yang kami digunakan adalah orang orang yang bekerja secara profesianal dan dapat dipercaya</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 highlight">
					<div class="h-caption"><h4><i class="fa fa-smile-o fa-5"></i>Pemecahan Solusi Anda</h4></div>
					<div class="h-body text-center">
						<p> Budi Setiawan Enterprise Transportindo adalah solusi dikala anda bingung memilih jasa logistik yang tepat, karena Budi Setiawan Enterprise Transportindo memiliki karyawan dan kurir yang bekerja secara profesional</p>
					</div>
				</div>
			</div> <!-- /row  -->
		</div>
	</div>
	<!-- /Highlights -->

	<div class="container text-center">
		<br> <br>

		<h2 class="thin">Visi dan Misi Perusahaan</h2>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<h2>Visi</h2>
				<div class="h-body text-left">
					<ul>
						<li>Menjadikan perusahaan yang kompetitif dan berorientasi pada kebaikan bersama.</li>
						<li>Terdepan dalam pelayanan yang berorientasi pada kepuasan partner.</li>
					</ul>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<h2>Misi</h2>
				<div class="h-body text-left">
					<ol>
						<li>Terbinannya kerjasama yang professional dengan semua pemamngku kepentingan (stakeholder).</li>
						<li>Sebagai perusahaan yang mampu dan bisa berkembang sesuai kebutuhan.</li>
						<li>Menjadi ruang untuk pengembangan usaha secara berkesinambungan.</li>
						<li>Melakukan kegiatan bisnis dengan memperhatikan tata kelola perusahaan yang baik (good corporate government).</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<div class="jumbotron top-space">
		<div class="container">
			
			<h3 class="text-center thin" style="font-weight: bold; margin-bottom: 30px;">Sejarah Perusahaan Kami</h3>
			<p class="text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, itaque, cumque, maxime obcaecati reprehenderit ea dignissimos amet voluptatem id excepturi facilis totam veritatis maiores eveniet neque explicabo temporibus quisquam in ex ab fugiat ipsa tempore sunt corporis nostrum quam illum!</p>
		
		</div>
	</div>

	<div class="container text-center">
		<br> <br>

		<h2 class="thin">Struktur Perusahaan</h2>
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<!-- <h2>Visi</h2>
				<div class="h-body text-left">
					<ul>
						<li>Menjadikan perusahaan yang kompetitif dan berorientasi pada kebaikan bersama.</li>
						<li>Terdepan dalam pelayanan yang berorientasi pada kepuasan partner.</li>
					</ul>
				</div> -->
			</div>
			<div class="col-md-6 col-sm-6">
				<!-- <h2>Misi</h2>
				<div class="h-body text-left">
					<ol>
						<li>Terbinannya kerjasama yang professional dengan semua pemamngku kepentingan (stakeholder).</li>
						<li>Sebagai perusahaan yang mampu dan bisa berkembang sesuai kebutuhan.</li>
						<li>Menjadi ruang untuk pengembangan usaha secara berkesinambungan.</li>
						<li>Melakukan kegiatan bisnis dengan memperhatikan tata kelola perusahaan yang baik (good corporate government).</li>
					</ol>
				</div> -->
			</div>
		</div>
	</div>

	<div class="jumbotron top-space">
		<div class="container">
			
			<h3 class="text-center thin" style="font-weight: bold">Berita Perusahaan Kami</h3>
			
			<div class="row">
			<?php 
		        $no = 1;
		        foreach($data as $d){ 
		    ?>
				<div class="col-md-3 col-sm-6 highlight">
					<a href="berita/<?php echo $d->ID_BERITA ?>">
						<div class="h-caption"><h4><img height="64" src="<?php echo base_url() ?>assets/images/berita/<?php echo $d->GAMBAR ?>" alt="No Image"></h4></div>
						<div class="h-caption"><h4><?php echo $d->JUDUL ?></h4></div>
					</a>
					<div class="h-body text-justify">
						<p><?php echo substr(strip_tags($d->ISI), 0, 128) ?>...<br><a href="berita/<?php echo $d->ID_BERITA ?>">read more</a></p>
					</div>
				</div>
			<?php $no++; } ?>
			</div> <!-- /row  -->
		</div>
	</div>