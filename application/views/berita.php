  <body>
        <header style="height: 100px;" id="hero-area" data-stellar-background-ratio="0.5">    
                <!-- Navbar Start -->
                <nav class="navbar navbar-expand-lg scrolling-navbar fixed-top indigo">
                  <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <a href="<?php echo base_url() ?>" class="navbar-brand"><img class="img-fulid" src="<?php echo base_url() ?>assets/images/logo.png" height="40px" alt=""></a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="lnr lnr-menu"></i>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar">
                      <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>galeri">Galeri</a>
                        </li>
                      </ul>
                    </div>
                  </div>
          
                  <!-- Mobile Menu Start -->
                  <ul class="mobile-menu">
                     <li>
                        <a class="page-scroll" href="<?php echo base_url() ?>">Home</a>
                      </li>
                      <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>galeri">Galeri</a>
                        </li>
                  </ul>
                  <!-- Mobile Menu End -->
          
                </nav>
                <!-- Navbar End -->           
              </header>

              <section id="blog" class="section" style="min-height: 450px;">
                    <!-- Container Starts -->
                    <div class="container">
                      <div class="section-header">          
                        <h2 class="section-title">Berita Perusahaan</h2>
                        <hr class="lines">
                      </div>
                      <div class="row">
                      <?php
		                    foreach($data as $d){ 
		                  ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 blog-item" style="margin-bottom: 24px">
                          <!-- Blog Item Starts -->
                          <div class="blog-item-wrapper">
                            <div class="blog-item-img">
                              <a href="<?php echo base_url() ?>berita/detail/<?php echo $d->SLUG ?>">
                                <img style="max-height:278px;width:auto;max-width:100%" src="<?php echo base_url() ?>assets/images/berita/<?php echo $d->GAMBAR ?>" alt="">
                              </a>                
                            </div>
                            <div class="blog-item-text"> 
                              <div class="meta-tags">
                                <span class="date"><i class="lnr lnr-calendar-full"></i> <?php echo date_format(date_create($d->CREATE_AT),"d M Y") ?></span>
                                <!-- <span class="comments"><a href="#"><i class="lnr lnr-bubble"></i> 24 Comments</a></span> -->
                              </div>
                              <h3>
                                <a href="<?php echo base_url() ?>berita/detail/<?php echo $d->SLUG ?>"><?php echo $d->JUDUL ?></a>
                              </h3>
                              <p>
                              <?php echo substr(strip_tags($d->ISI), 0, 128) ?>...
                              </p>
                              <a href="<?php echo base_url() ?>berita/detail/<?php echo $d->SLUG ?>" class="btn-rm">Read More <i class="lnr lnr-arrow-right"></i></a>
                            </div>
                          </div>
                          <!-- Blog Item Wrapper Ends-->
                        </div>
                      <?php } ?>
                      </div>
                    </div>
                  </section>
                  <section class="section">  
      <div class="container">
        <div class="row">
          <?php echo $paging ?>
        </div>
      </div>
    </section>