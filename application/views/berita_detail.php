<body>
          <header style="height: 100px;" id="hero-area" data-stellar-background-ratio="0.5">    
                <!-- Navbar Start -->
                <nav class="navbar navbar-expand-lg scrolling-navbar fixed-top indigo">
                  <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <a href="<?php echo base_url() ?>" class="navbar-brand"><img class="img-fulid" src="<?php echo base_url() ?>assets/images/logo.png" height="40px" alt=""></a>
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="lnr lnr-menu"></i>
                      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="main-navbar">
                      <ul class="navbar-nav mr-auto w-100 justify-content-end">
                        <li class="nav-item">
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>berita">Berita</a>
                        </li>
                        <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>galeri">Galeri</a>
                        </li>
                    </div>
                  </div>
          
                  <!-- Mobile Menu Start -->
                  <ul class="mobile-menu">
                     <li>
                        <a class="page-scroll" href="<?php echo base_url() ?>">Home</a>
                      </li>
                      <li>
                        <a class="page-scroll" href="<?php echo base_url() ?>berita">Berita</a>
                      </li>
                      <li>
                          <a class="nav-link page-scroll" href="<?php echo base_url() ?>galeri">Galeri</a>
                        </li>
                  </ul>
                  <!-- Mobile Menu End -->
          
                </nav>
                <!-- Navbar End -->   
                <div class="container">      
                  <div class="row justify-content-md-center">
                    <div class="col-md-10">
                      <div class="contents text-center" style="height: 40px;">
                      </div>
                    </div>
                  </div> 
                </div>           
              </header>

              <section id="portfolios" class="section" style="min-height: 900px;">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-lg-8">
                    <?php foreach($data as $d){ ?>
                      <div class="text-center">
                        <h2 class="zoomIn" style="margin-bottom: 25px;" data-wow-duration="1000ms" data-wow-delay="100ms" ><?php echo $d->JUDUL ?></h2>
                        <div class="portfolio-item">
                        <?php if($d->GAMBAR){ ?>
                          <div class="shot-item" style="height: auto;">
                            <img style="max-height:278px;width:auto" src="<?php echo base_url() ?>assets/images/berita/<?php echo $d->GAMBAR ?>" alt="">
                            <a class="overlay lightbox" href="<?php echo base_url() ?>assets/images/berita/<?php echo   $d->GAMBAR ?>">
                              <i class="lnr lnr-eye item-icon"></i>
                            </a>
                          </div>
                        <?php } ?>
                        </div>
                        <div class="meta-tags" style="text-align: left">
                          <span class="date"><i class="lnr lnr-calendar-full"></i> <?php echo date_format(date_create ($d->CREATE_AT),"d M Y") ?></span>
                        </div>
                        <div style="text-align: justify">
                          <?php echo $d->ISI ?>
                        </div>
                      </div>
                    <?php } ?>
                    </div>
                  </div>
                </div>
              </section>
            