<!-- Footer Section Start -->
    <footer>          
      <div class="container">
        <div class="row">
          <!-- Footer Links -->
          <div class="col-12">
            <div class="copyright" >
              <p>All copyrights reserved &copy; 2018 - BSE Transportindo</p>
            </div>
          </div>  
        </div>
      </div>
    </footer>
    <!-- Footer Section End --> 

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="lnr lnr-arrow-up"></i>
    </a>
    
    <div id="loader">
      <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
      </div>
    </div>     

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="<?php echo base_url() ?>assets/js/jquery-min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/bootstrap-index.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.mixitup.js"></script>
    <script src="<?php echo base_url() ?>assets/js/nivo-lightbox.js"></script>
    <script src="<?php echo base_url() ?>assets/js/owl.carousel.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/jquery.stellar.min.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/jquery.nav.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/scrolling-nav.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/smoothscroll.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/jquery.slicknav.js"></script>     
    <script src="<?php echo base_url() ?>assets/js/wow.js"></script>   
    <script src="<?php echo base_url() ?>assets/js/jquery.vide.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.counterup.min.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/jquery.magnific-popup.min.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/waypoints.min.js"></script>    
    <script src="<?php echo base_url() ?>assets/js/form-validator.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/contact-form-script.js"></script>   
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>

  </body>
</html>