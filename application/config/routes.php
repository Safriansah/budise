<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'IndexControl';
$route['404_override'] = 'ErrorControl';
$route['translate_uri_dashes'] = FALSE;
$route['berita'] = 'IndexControl/berita';
$route['simpan-pertanyaan'] = 'IndexControl/insert_pertanyaan';
$route['berita/detail/(:any)'] = 'IndexControl/detail_berita/$1';
$route['berita/halaman/([0-9]+)'] = 'IndexControl/berita_next/$1';
$route['galeri'] = 'IndexControl/galeri';
$route['galeri/halaman/([0-9]+)'] = 'IndexControl/galeri_next/$1';
$route['admin'] = 'AdminControl';
$route['admin/profile'] = 'AdminControl/profile';
$route['admin/profile/update'] = 'AdminControl/update_profile';
$route['admin/profile/update_user'] = 'AdminControl/update_user_profile';
$route['admin/login'] = 'LoginControl';
$route['admin/logout'] = 'LoginControl/logout';
$route['admin/logcek'] = 'LoginControl/login';
$route['admin/pegawai'] = 'PegawaiControl';
$route['admin/pegawai/([a-z]+)'] = 'PegawaiControl/$1';
$route['admin/user'] = 'UserControl';
$route['admin/user/([a-z]+)'] = 'UserControl/$1';
$route['admin/berita'] = 'BeritaControl';
$route['admin/berita/([a-z]+)'] = 'BeritaControl/$1';
$route['admin/order'] = 'OrderControl';
$route['admin/order/([a-z]+)'] = 'OrderControl/$1';
$route['admin/pertanyaan'] = 'PertanyaanControl';
$route['admin/pertanyaan/([a-z]+)'] = 'PertanyaanControl/$1';
$route['admin/galeri'] = 'GaleriControl';
$route['admin/galeri/([a-z]+)'] = 'GaleriControl/$1';