<?php 
 
class PegawaiControl extends CI_Controller
{	
	var $table="PEGAWAI";
	var $menu = "1";

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login" || $this->session->userdata('role')>1){
			redirect(base_url("admin/login"));
		}		
		$this->load->model('m_data');
	}

	function index(){
		$data['data'] = $this->m_data->tampil_data($this->table)->result();
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/pegawai_view');
        $this->load->view('admin/footer');
	}

	function insert(){
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$gender = $this->input->post('gender');
		$lahir = $this->input->post('lahir');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$jabatan = $this->input->post('jabatan');
 
		$data = array(
			'nip' => $nip,
			'nama' => $nama,
			'gender' => $gender,
			'tgl_lahir' => $lahir,
			'alamat' => $alamat,
			'telp' => $telp,
			'email' => $email,
			'jabatan' => $jabatan
		);

		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->input_data($data,$this->table),
				'simpan'
			)
		);
		
		redirect('admin/pegawai');
	}

	function edit(){
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('ID_PGW' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
        $this->load->view('admin/pegawai_modal_edit',$data);
	}
 
	function update(){
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$gender = $this->input->post('gender');
		$lahir = $this->input->post('lahir');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$jabatan = $this->input->post('jabatan');
 
		$data = array(
			'nama' => $nama,
			'gender' => $gender,
			'tgl_lahir' => $lahir,
			'alamat' => $alamat,
			'telp' => $telp,
			'email' => $email,
			'jabatan' => $jabatan
		);

		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->update_data(array('nip' => $nip),$data,$this->table),
				'simpan'
			)
		);

		redirect('admin/pegawai');
	}
	
	function hapus(){
		$id = $this->input->post('id');
		$where = array('ID_PGW' => $id);
		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->hapus_data($where,$this->table),
				'hapus'
			)
		);
		redirect('admin/pegawai');
	}
}