<?php 
 
class UserControl extends CI_Controller
{	
	var $table="USER";
	var $menu = "2";

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login" || $this->session->userdata('role')>1){
			redirect(base_url("admin/login"));
		}		
		$this->load->model('m_data');
		$this->load->model('m_user');
	}

	function index(){
		$data['data'] = $this->m_user->tampil_data_user()->result();
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/user_view');
        $this->load->view('admin/footer');
	}
 
	function tambah(){
		$id = $this->input->post('id');
		$nip = $this->input->post('nip');
		$lahir = $this->input->post('lahir');
		list($thn,$bln,$tgl) = explode( "-", $lahir );
		$pass=$tgl.$bln.$thn;
 
		if($this->m_data->isExist('USER',array('username'=>$nip))){
			$this->session->set_flashdata('notif','<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Username tidak tersedia.</div>');
			return redirect('admin/pegawai');
		}

		$data = array(
			'username' => $nip,
			'id_pgw' => $id,
			'password' => md5($pass),
			'role'=>'2'
		);
		
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_user->insert_user($data, $id),'simpan'));
		redirect('admin/pegawai');
	}

	function edit(){
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('ID_USER' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
        $this->load->view('admin/user_modal_edit',$data);
	}

	function update(){
		$user = $this->input->post('username');
		$old = $this->input->post('userold');
		$id = $this->input->post('id');
		$pass = $this->input->post('password');
		$tipe = $this->input->post('tipe');
		
		if($user!=$old)
			if($this->m_data->isExist('USER',array('username'=>$user))){
				$this->session->set_flashdata('notif','<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Username tidak tersedia.</div>');
				return redirect('admin/user');
			}

		if($pass)
		$data = array(
			'username' => $user,
			'password' => md5($pass),
			'role' => $tipe
		);

		else
		$data = array(
			'username' => $user,
			'role' => $tipe
		);

		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->update_data(array('id_user' => $id),$data,$this->table),'simpan'));
		
		redirect('admin/user');
	}

	function hapus(){
		$id = $this->input->post('id');
		$idp = $this->input->post('idp');
		$where = array('ID_USER' => $id);
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_user->hapus_data($where,$this->table),'hapus'));
		redirect('admin/user');
	}
}