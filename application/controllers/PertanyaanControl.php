<?php 
 
class pertanyaanControl extends CI_Controller
{	
	var $table="PERTANYAAN";
	var $menu = "4";

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login" || $this->session->userdata('role')>2){
			redirect(base_url("admin/login"));
		}		
		$this->load->model('m_data');
	}

	function index(){
		$data['data'] = $this->m_data->tampil_data($this->table)->result();
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/pertanyaan_view');
        $this->load->view('admin/footer');
	}

	function insert(){
		$nama = $this->input->post('nama');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$pertanyaan = $this->input->post('pertanyaan');
		$status = $this->input->post('status');
 
		$data = array(
			'id_pgw'=>$this->session->userdata('id'),
			'nama' => $nama,
			'telp' => $telp,
			'email' => $email,
			'pertanyaan' => $pertanyaan,
			'status' => $status,
			'create_at' => date('Y-m-d H:i:s')
		);

		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->input_data($data,$this->table),
				'simpan'
			)
		);
		redirect('admin/pertanyaan');
	}

	function edit(){
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('ID_PERTANYAAN' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
		if($data['data'][0]->STATUS!=0)
			$data['crby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->ID_PGW));
        $this->load->view('admin/pertanyaan_modal_edit',$data);
	}
 
	function update(){
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$pertanyaan = $this->input->post('pertanyaan');
		$status = $this->input->post('status');
 
		$data = array(
			'id_pgw'=>$this->session->userdata('id'),
			'nama' => $nama,
			'telp' => $telp,
			'email' => $email,
			'pertanyaan' => $pertanyaan,
			'status' => $status
		);

		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->update_data(array('ID_PERTANYAAN' => $id),$data,$this->table),
				'simpan'
			)
		);
		redirect('admin/pertanyaan');
	}

	function hapus(){
		$id = $this->input->post('id');
		$where = array('ID_PERTANYAAN' => $id);
		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->hapus_data($where,$this->table),
				'hapus'
			)
		);
		redirect('admin/pertanyaan');
	}
}