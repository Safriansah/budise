<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndexControl extends CI_Controller 
{
    function __construct(){
		parent::__construct();		
        $this->load->model('m_data');
        $this->load->model('m_beranda');
        $this->tampil="9";
    }
    
    public function index()
	{
        $data['data'] = $this->m_beranda->tampil_limit('BERITA',3,'id_berita')->result();
        $data['countberita']=$this->m_beranda->jumlah('BERITA');
        $data['galeri'] = $this->m_beranda->tampil_limit('GALERI',6,'id_galeri')->result();
        $data['countgaleri']=$this->m_beranda->jumlah('GALERI');
        $data['pemesanan'] = $this->m_beranda->get_pesan_selesai();
        $data['klien'] = $this->m_beranda->get_klien();
        $data['title'] = "";
        $this->load->view('header',$data);
        $this->load->view('index');
        $this->load->view('footer');
    }

    public function berita()
	{
        $start=0;
        $tampil=$this->tampil;
        $total=$this->m_beranda->jumlah('BERITA');
        $data['data'] = $this->m_beranda->tampil_range('BERITA', $start, $tampil, 'id_berita')->result();
        $data['title'] = "Berita - ";
        $data['paging']='';
        if($start+$tampil<$total) 
            $data['paging']='   <div class="col-sm-4"></div>
                                <div class="col-sm-4" style="text-align:center;font-size:14;padding-top:14px">Halaman 1</div>
                                <div class="col-sm-4" style="text-align:right">
                                    <a href="'.base_url().'berita/halaman/2">
                                        <div class="btn btn-common"><span class="lnr lnr-arrow-right"></span></div>
                                    </a>
                                </div>';
        else
            $data['paging']='   <div class="col-sm-4"></div>
                                <div class="col-sm-4" style="text-align:center;font-size:14;padding-top:14px">Halaman 1</div>
                                <div class="col-sm-4"></div>';

        $this->load->view('header',$data);
        $this->load->view('berita');
        $this->load->view('footer');
    }

    public function berita_next($id)
	{
        if($id<2) redirect(base_url("berita"));
        $tampil=$this->tampil;
        $start=($id-1)*$tampil;
        $total=$this->m_beranda->jumlah('BERITA');
        $prev=$id-1;
        $next=$id+1;

        $data['paging']='<div class="col-sm-4">
                            <a href="'.base_url().'berita/halaman/'.$prev.'">
                                <div class="btn btn-common"><span class="lnr lnr-arrow-left"></span></div>
                            </a>
                        </div>
                        <div class="col-sm-4" style="text-align:center;font-size:14;padding-top:14px">Halaman '.$id.'</div>';
        
        if($start+$tampil<$total) 
            $data['paging'].='  <div class="col-sm-4" style="text-align:right">
                                    <a href="'.base_url().'berita/halaman/'.$next.'">
                                        <div class="btn btn-common"><span class="lnr lnr-arrow-right"></span></div>
                                    </a>
                                </div>';
        
        $data['data'] = $this->m_beranda->tampil_range('BERITA', $start, $tampil, 'id_berita')->result();
        $data['title'] = "Berita - ";
        
        $this->load->view('header',$data);
        $this->load->view('berita');
        $this->load->view('footer');
    }

    public function detail_berita($id)
	{
		$where = array('slug' => $id);
        $data['data'] = $this->m_beranda->edit_data($where,'BERITA')->result();
        $this->m_beranda->set_populer($where,'BERITA');
        $data['title'] = "Berita - ";
        $this->load->view('header',$data);
        $this->load->view('berita_detail');
        $this->load->view('footer');
    }

    function insert_pertanyaan(){
		$nama = $this->input->post('name');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$pertanyaan = $this->input->post('message');
		$status = '0';
 
		$data = array(
			'nama' => $nama,
			'telp' => $telp,
			'email' => $email,
			'pertanyaan' => $pertanyaan,
			'status' => $status,
			'create_at' => date('Y-m-d H:i:s')
		);
        
        if($this->m_data->input_data($data,'PERTANYAAN')) echo "success";
        else echo "error";
    }
    
    public function galeri()
	{
        $start=0;
        $tampil=$this->tampil+3;
        $total=$this->m_beranda->jumlah('GALERI');
        $data['data'] = $this->m_beranda->tampil_range('GALERI', $start, $tampil, 'id_galeri')->result();
        $data['title'] = "Galeri - ";
        $data['paging']='';
        if($start+$tampil<$total) 
            $data['paging']='   <div class="col-sm-4"></div>
                                <div class="col-sm-4" style="text-align:center;font-size:14;padding-top:14px">Halaman 1</div>
                                <div class="col-sm-4" style="text-align:right">
                                    <a href="'.base_url().'galeri/halaman/2">
                                        <div class="btn btn-common"><span class="lnr lnr-arrow-right"></span></div>
                                    </a>
                                </div>';
        else
            $data['paging']='   <div class="col-sm-4"></div>
                                <div class="col-sm-4" style="text-align:center;font-size:14;padding-top:14px">Halaman 1</div>
                                <div class="col-sm-4"></div>';

        $this->load->view('header',$data);
        $this->load->view('galeri');
        $this->load->view('footer');
    }

    public function galeri_next($id)
	{
        if($id<2) redirect(base_url("galeri"));
        $tampil=$this->tampil+3;
        $start=($id-1)*$tampil;
        $total=$this->m_beranda->jumlah('GALERI');
        $prev=$id-1;
        $next=$id+1;

        $data['paging']='<div class="col-sm-4">
                            <a href="'.base_url().'galeri/halaman/'.$prev.'">
                                <div class="btn btn-common"><span class="lnr lnr-arrow-left"></span></div>
                            </a>
                        </div>
                        <div class="col-sm-4" style="text-align:center;font-size:14;padding-top:14px">Halaman '.$id.'</div>';
        
        if($start+$tampil<$total) 
            $data['paging'].='  <div class="col-sm-4" style="text-align:right">
                                    <a href="'.base_url().'galeri/halaman/'.$next.'">
                                        <div class="btn btn-common"><span class="lnr lnr-arrow-right"></span></div>
                                    </a>
                                </div>';
        
        $data['data'] = $this->m_beranda->tampil_range('GALERI', $start, $tampil, 'id_galeri')->result();
        $data['title'] = "Galeri - ";
        
        $this->load->view('header',$data);
        $this->load->view('galeri');
        $this->load->view('footer');
    }
}