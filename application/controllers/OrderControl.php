<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderControl extends CI_Controller 
{
    var $menu = "3";
    var $table='PEMESANAN';
    function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login" || $this->session->userdata('role')>2){
			redirect(base_url("admin/login"));
		}
        $this->load->model('m_data');
        $this->load->model('m_order');
    }
    
    public function index()
	{
        $data['menu'] = $this->menu;
        $data['data'] = $this->m_data->tampil_data($this->table)->result();
        $this->load->view('admin/header',$data);
        $this->load->view('admin/order_view');
        $this->load->view('admin/footer');
    }

    public function create()
	{
        $data['menu'] = $this->menu;
        $data['kode'] = $this->m_order->get_kode();
        $this->load->view('admin/order_modal_add',$data);
    }

    function insert(){
        $kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $telp = $this->input->post('telp');
        $email = $this->input->post('email');
        $pengirim = $this->input->post('pengirim');
        $alamat_kirim = $this->input->post('alamat_kirim');
        $penerima = $this->input->post('penerima');
        $alamat_penerima = $this->input->post('alamat_penerima');
        $tgl_kirim = $this->input->post('tgl_kirim');
        $harga = $this->input->post('harga');
        $status = $this->input->post('status');
 
		$data = array(
			'id_pgw'=>$this->session->userdata('id'),
            'KODE_PESAN'=>$kode,
			'pemesan'=>$nama,
			'telp_pemesan' => $telp,
            'email_pemesan' => $email,
            'pengirim' => $pengirim,
            'alamat_kirim' => $alamat_kirim,
            'penerima' => $penerima,
            'alamat_penerima' => $alamat_penerima,
            'tgl_kirim' => $tgl_kirim,
            'total_harga' => $harga,
            'status'=>$status,
            'create_at' => date('Y-m-d H:i:s'),
			'create_by'=>$this->session->userdata('id')
		);
        
		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->input_data($data,$this->table),
				'simpan'
			)
		);
		redirect('admin/order');
    }

    public function detail()
    {
        $data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('id_pesan' => $id);
        $data['data'] = $this->m_data->edit_data($where,$this->table)->result();
        $data['crby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->CREATE_BY));
        $data['upby'] = '';
        if($data['data'][0]->UPDATE_BY){
            $data['upby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->UPDATE_BY));
        }   
        $this->load->view('admin/order_modal_detail',$data);
    }
    
    public function edit()
    {
        $data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('id_pesan' => $id);
        $data['data'] = $this->m_data->edit_data($where,$this->table)->result();
        $this->load->view('admin/order_modal_edit',$data);
    }

    function update(){
        $kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $telp = $this->input->post('telp');
        $email = $this->input->post('email');
        $pengirim = $this->input->post('pengirim');
        $alamat_kirim = $this->input->post('alamat_kirim');
        $penerima = $this->input->post('penerima');
        $alamat_penerima = $this->input->post('alamat_penerima');
        $tgl_kirim = $this->input->post('tgl_kirim');
        $harga = $this->input->post('harga');
        $status = $this->input->post('status');
 
		$data = array(
			'pemesan'=>$nama,
			'telp_pemesan' => $telp,
            'email_pemesan' => $email,
            'pengirim' => $pengirim,
            'alamat_kirim' => $alamat_kirim,
            'penerima' => $penerima,
            'alamat_penerima' => $alamat_penerima,
            'tgl_kirim' => $tgl_kirim,
            'total_harga' => $harga,
            'status'=>$status,
            'update_at' => date('Y-m-d H:i:s'),
			'update_by'=>$this->session->userdata('id')
		);
        
		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->update_data(array('KODE_PESAN'=>$kode),$data,$this->table),
				'simpan'
			)
		);
		redirect('admin/order');
    }

    function hapus(){
		$id = $this->input->post('id');
		$where = array('id_pesan' => $id);
		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->hapus_data($where,$this->table),
				'hapus'
			)
		);
		redirect('admin/order');
	}
}