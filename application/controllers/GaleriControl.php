<?php 
 
class GaleriControl extends CI_Controller
{	
	var $table="GALERI";
	var $menu = "6";
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("admin/login"));
		}		
		$this->load->model('m_data');
		$this->load->library('upload');
	}

	function index(){
		$data['data'] = $this->m_data->tampil_data($this->table)->result();
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/galeri_view');
		$this->load->view('admin/footer');
	}
 
	function tambah(){
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
        $this->load->view('admin/galeri_add');
        $this->load->view('admin/footer');
	}

	function insert(){
		$judul = $this->input->post('judul');
		$deskripsi = $this->input->post('deskripsi');
 
		$data = array(
			'id_pgw'=>$this->session->userdata('id'),
			'judul' => $judul,
			'deskripsi' => $deskripsi,
			'create_at' => date('Y-m-d H:i:s'),
			'create_by'=>$this->session->userdata('id')
		);
		
		$config['file_name'] = date('Y-m-d H:i:s'); //nama yang terupload nantinya
		$config['upload_path'] = 'assets/images/galeri/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
		$this->upload->initialize($config);
		
		if(!empty($_FILES['gambar']['name'])){
            if ($this->upload->do_upload('gambar')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='assets/images/galeri/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= 'assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
				$data = array(
					'id_pgw'=>$this->session->userdata('id'),
					'judul' => $judul,
					'deskripsi' => $deskripsi,
					'gambar' => $gbr['file_name'],
					'create_at' => date('Y-m-d H:i:s'),
					'create_by'=>$this->session->userdata('id')
				);
			}
			else echo $this->upload->display_errors().$config['upload_path'];
		}
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->input_data($data,$this->table),'simpan'));
		redirect('admin/galeri');
	}

	public function detail()
    {
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('id_galeri' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
		$data['crby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->CREATE_BY));
        $data['upby'] = '';
        if($data['data'][0]->UPDATE_BY){
            $data['upby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->UPDATE_BY));
        }  
        $this->load->view('admin/galeri_modal_detail',$data);
    }

	function edit(){
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('id_galeri' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
        $this->load->view('admin/galeri_modal_edit',$data);
	}
 
	function update(){
		$judul = $this->input->post('judul');
		$deskripsi = $this->input->post('deskripsi');
		$id=$this->input->post('id');
 
		$data = array(
			'judul' => $judul,
			'deskripsi' => $deskripsi,
			'update_at' => date('Y-m-d H:i:s'),
			'update_by'=>$this->session->userdata('id')
		);
		
		$item = $this->m_data->edit_data(array('id_galeri' => $id),$this->table)->result();
		if(!empty($_FILES['gambar']['name']))
			if($item[0]->GAMBAR) unlink("assets/images/galeri/".$item[0]->GAMBAR);

		$config['file_name'] = date('Y-m-d H:i:s'); //nama yang terupload nantinya
		$config['upload_path'] = 'assets/images/galeri/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
		$this->upload->initialize($config);

		if(!empty($_FILES['gambar']['name'])){
            if($this->upload->do_upload('gambar')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='assets/images/galeri/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= 'assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
				$data = array(
					'judul' => $judul,
					'deskripsi' => $deskripsi,
					'gambar' => $gbr['file_name'],
					'update_at' => date('Y-m-d H:i:s'),
					'update_by'=>$this->session->userdata('id')
				);
			}
			else echo $this->upload->display_errors().$config['upload_path'];
		}
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->update_data(array('id_galeri' => $id),$data,$this->table),'simpan'));
		redirect('admin/galeri');
	}

	function hapus(){
		$id = $this->input->post('id');
		$where = array('id_galeri' => $id);
		$item = $this->m_data->edit_data($where,$this->table)->result();
		if($item[0]->GAMBAR) unlink("assets/images/galeri/".$item[0]->GAMBAR);
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->hapus_data($where,$this->table),'hapus'));
		redirect('admin/galeri');
	}


}