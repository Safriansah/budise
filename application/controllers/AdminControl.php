<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminControl extends CI_Controller 
{
    var $menu = "0";
    function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("admin/login"));
		}
        $this->load->model('m_data');
    }
    
    public function index()
	{
		$data['berita'] = $this->m_data->jumlah('BERITA');
		$data['pgw'] = $this->m_data->jumlah('PEGAWAI');
		$data['pesan'] = $this->m_data->jumlah('PEMESANAN');
		$data['tanya'] = $this->m_data->jumlah('PERTANYAAN');
		$data['galeri'] = $this->m_data->jumlah('GALERI');
        $data['menu'] = $this->menu;
        $this->load->view('admin/header',$data);
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer');
    }

	function profile(){
		$data['menu'] = '-';
		$id=$this->session->userdata('id');
		$where = array('ID_PGW' => $id);
		$data['data'] = $this->m_data->edit_data($where,'PEGAWAI')->result();
		$data['user'] = $this->m_data->edit_data($where,'USER')->result();
        $this->load->view('admin/header',$data);
        $this->load->view('admin/profile');
        $this->load->view('admin/footer');
	}

	function update_profile(){
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$gender = $this->input->post('gender');
		$lahir = $this->input->post('lahir');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
 
		$data = array(
			'nama' => $nama,
			'gender' => $gender,
			'tgl_lahir' => $lahir,
			'alamat' => $alamat,
			'telp' => $telp,
			'email' => $email
		);

		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$this->m_data->update_data(array('nip' => $nip),$data,'PEGAWAI'),
				'simpan'
			)
		);
		redirect('admin/profile');
	}

	function update_user_profile(){
		$username = $this->input->post('username');
		$id = $this->input->post('id');
		$password = $this->input->post('password');
		
		if($username!=$this->session->userdata('user')){
			if($this->m_data->isExist('USER',array('username'=>$username))){
				$this->session->set_flashdata('notif','<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> Username sudah ada yang pakai.</div>');
				redirect('admin/profile');
			}
		}

		if($password)
		$data = array(
			'username' => $username,
			'password' => md5($password)
		);

		else
		$data = array(
			'username' => $username
		);

		$note=$this->m_data->update_data(array('id_pgw' => $id),$data,'USER');
		$this->session->set_flashdata(
			'notif',
			$this->m_data->get_notif(
				$note,
				'simpan'
			)
		);
		if($note) $this->session->set_userdata('user', $username);

		redirect('admin/profile');
	}
}