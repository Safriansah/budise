<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginControl extends CI_Controller 
{
	var $tabel = "USER";
    function __construct(){
        parent::__construct();
        $this->load->model('m_data');
    }
    
    public function index()
	{
        if($this->session->userdata('status') == "login"){
			redirect(base_url("admin"));
		}
        $this->load->view('admin/login');
    }

    function login(){
		$username = $this->input->post('id');
		$password = $this->input->post('pass');
		$where = array(
			'USERNAME' => $username,
			'PASSWORD' => md5($password)
			);
        $cek = $this->m_data->edit_data($where,$this->tabel)->num_rows();
		if($cek > 0){
            
            $item = $this->m_data->edit_data($where,$this->tabel)->result();
			$data_session = array(
                'role'=>$item[0]->ROLE,
				'user' => $item[0]->USERNAME,
				'id' => $item[0]->ID_PGW,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
 
			redirect(base_url("admin"));
 
		}else{
            $this->session->set_flashdata('notif','<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Gagal!</strong> Username atau password salah.</div>');
            redirect(base_url("admin/login"));
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect('admin/login');
	}
}