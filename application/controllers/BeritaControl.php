<?php 
 
class BeritaControl extends CI_Controller
{	
	var $table="BERITA";
	var $menu = "5";
	
	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("admin/login"));
		}		
		$this->load->model('m_data');
		$this->load->library('upload');
	}

	function index(){
		$data['data'] = $this->m_data->tampil_data($this->table)->result();
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
		$this->load->view('admin/berita_view');
		$this->load->view('admin/footer');
	}
 
	function tambah(){
		$data['menu'] = $this->menu;
		$this->load->view('admin/header',$data);
        $this->load->view('admin/berita_add');
        $this->load->view('admin/footer');
	}

	function insert(){
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$url=$this->input->post('url');

		if($this->m_data->isExist($this->table, array('slug'=>$url))){
			$this->session->set_flashdata('notif','<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> URL tidak tersedia.</div>');
			return redirect('admin/berita/tambah');
		}
 
		$data = array(
			'id_pgw'=>$this->session->userdata('id'),
			'isi' => $isi,
			'judul' => $judul,
			'tampil' => '0',
			'slug'=>$url,
			'create_at' => date('Y-m-d H:i:s'),
			'create_by'=>$this->session->userdata('id')
		);
		
		$config['file_name'] = date('Y-m-d H:i:s'); //nama yang terupload nantinya
		$config['upload_path'] = 'assets/images/berita/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
		$this->upload->initialize($config);
		
		if(!empty($_FILES['gambar']['name'])){
            if ($this->upload->do_upload('gambar')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='assets/images/berita/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= 'assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
				$data = array(
					'id_pgw'=>$this->session->userdata('id'),
					'isi' => $isi,
					'judul' => $judul,
					'tampil' => '0',
					'slug'=>$url,
					'gambar' => $gbr['file_name'],
					'create_at' => date('Y-m-d H:i:s'),
					'create_by'=>$this->session->userdata('id')
				);
			}
			else echo $this->upload->display_errors().$config['upload_path'];
		}
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->input_data($data,$this->table),'simpan'));
		redirect('admin/berita/tambah');
	}

	public function detail()
    {
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('id_berita' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
		$data['crby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->CREATE_BY));
        $data['upby'] = '';
        if($data['data'][0]->UPDATE_BY){
            $data['upby'] = $this->m_data->get_nama_pgw('PEGAWAI',array('ID_PGW'=>$data['data'][0]->UPDATE_BY));
        }  
        $this->load->view('admin/berita_modal_detail',$data);
    }

	function edit(){
		$data['menu'] = $this->menu;
		$id=$this->input->get('id');
		$where = array('id_berita' => $id);
		$data['data'] = $this->m_data->edit_data($where,$this->table)->result();
        $this->load->view('admin/berita_modal_edit',$data);
	}
 
	function update(){
		$judul = $this->input->post('judul');
		$isi = $this->input->post('isi');
		$id = $this->input->post('id');
		$url=$this->input->post('url');
		$old=$this->input->post('old');

		if($url!=$old)
			if($this->m_data->isExist($this->table, array('slug'=>$url))){
				$this->session->set_flashdata('notif','<div class="alert alert-warning alert-dismissable"><button type="button" 	class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Error!</strong> URL tidak 	tersedia.</div>');
				return redirect('admin/berita');
			}
 
		$data = array(
			'isi' => $isi,
			'judul' => $judul,
			'slug'=>$url,
			'update_at' => date('Y-m-d H:i:s'),
			'update_by'=>$this->session->userdata('id')
		);
		
		$item = $this->m_data->edit_data(array('id_berita' => $id),$this->table)->result();
		if(!empty($_FILES['gambar']['name']))
			if($item[0]->GAMBAR) unlink("assets/images/berita/".$item[0]->GAMBAR);

		$config['file_name'] = date('Y-m-d H:i:s'); //nama yang terupload nantinya
		$config['upload_path'] = 'assets/images/berita/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
 
		$this->upload->initialize($config);

		if(!empty($_FILES['gambar']['name'])){
            if($this->upload->do_upload('gambar')){
                $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='assets/images/berita/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                $config['width']= 600;
                $config['height']= 400;
                $config['new_image']= 'assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
				$data = array(
					'isi' => $isi,
					'judul' => $judul,
					'gambar' => $gbr['file_name'],
					'slug'=>$url,
					'update_at' => date('Y-m-d H:i:s'),
					'update_by'=>$this->session->userdata('id')
				);
			}
			else echo $this->upload->display_errors().$config['upload_path'];
		}
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->update_data(array('id_berita' => $id),$data,$this->table),'simpan'));
		redirect('admin/berita');
	}

	function hapus(){
		$id = $this->input->post('id');
		$where = array('id_berita' => $id);
		$item = $this->m_data->edit_data($where,$this->table)->result();
		if($item[0]->GAMBAR) unlink("assets/images/berita/".$item[0]->GAMBAR);
		$this->session->set_flashdata('notif',$this->m_data->get_notif($this->m_data->hapus_data($where,$this->table),'hapus'));
		redirect('admin/berita');
	}


}