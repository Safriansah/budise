<?php
 
class M_beranda extends M_data{
  	function tampil_limit($table,$limit,$col){
		$this->db->order_by($col, "desc"); 
		$this->db->limit($limit);
		return $this->db->get($table);
	}
	function tampil_range($table, $start, $all, $col){
		$this->db->order_by($col, "desc");
		return $this->db->get($table, $all, $start);
	}
	function get_populer($table){
		$this->db->order_by("tampil", "desc");
		$this->db->limit(5);
		return $this->db->get($table);
	}
	function set_populer($where,$table){
		$this->db->where($where);
		$this->db->set('TAMPIL', 'TAMPIL + 1', FALSE);
		$this->db->update($table);
	}
	function cari_berita($data,$table){
		$this->db->like('judul', $data); 
		$this->db->or_like('isi', $data);
		return $this->db->get($table);
	}
	function get_klien(){
		$hasil=$this->db->select('PEMESAN')->from('PEMESANAN')->group_by('TELP_PEMESAN')->get();
		return $hasil->num_rows();
	}
	function get_pesan_selesai(){
		$hasil=$this->db->select('PEMESAN')->from('PEMESANAN')->where(array('status'=>'3'))->get();
		return $hasil->num_rows();
	}
}