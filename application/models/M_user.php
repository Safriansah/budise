<?php
 
class M_user extends M_data{
	function insert_user($data, $id){
		$this->db->trans_start();
		$this->db->insert('USER', $data);
		$item = $this->m_user->edit_data($data,'USER')->result();
		$this->db->update('PEGAWAI',array('ID_USER' => $item[0]->ID_USER),array('ID_PGW' => $id));
		$this->db->trans_complete();

		if ($this->db->trans_status() == FALSE)
			return FALSE;
		else
			return TRUE;
	}
	function tampil_data_user(){
		$this->db->select('*'); // <-- There is never any reason to write this line!
		$this->db->from('USER');
		$this->db->join('PEGAWAI', 'USER.ID_PGW = PEGAWAI.ID_PGW');
		return $this->db->get();
	}
}