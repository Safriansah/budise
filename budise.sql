-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 06, 2018 at 08:35 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `budise`
--

-- --------------------------------------------------------

--
-- Table structure for table `BERITA`
--

CREATE TABLE `BERITA` (
  `ID_BERITA` int(11) NOT NULL,
  `ID_PGW` int(11) DEFAULT NULL,
  `JUDUL` varchar(255) DEFAULT NULL,
  `ISI` text,
  `GAMBAR` varchar(255) DEFAULT NULL,
  `SLUG` varchar(255) NOT NULL,
  `TAMPIL` int(11) DEFAULT NULL,
  `CREATE_AT` datetime DEFAULT NULL,
  `CREATE_BY` int(11) DEFAULT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `UPDATE_BY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `GALERI`
--

CREATE TABLE `GALERI` (
  `ID_GALERI` int(11) NOT NULL,
  `ID_PGW` int(11) DEFAULT NULL,
  `JUDUL` varchar(255) DEFAULT NULL,
  `GAMBAR` varchar(255) DEFAULT NULL,
  `DESKRIPSI` varchar(255) DEFAULT NULL,
  `CREATE_AT` datetime DEFAULT NULL,
  `CREATE_BY` int(11) DEFAULT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `UPDATE_BY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PEGAWAI`
--

CREATE TABLE `PEGAWAI` (
  `ID_PGW` int(11) NOT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `NIP` char(10) DEFAULT NULL,
  `NAMA` varchar(255) DEFAULT NULL,
  `GENDER` char(1) DEFAULT NULL,
  `TGL_LAHIR` date DEFAULT NULL,
  `ALAMAT` varchar(255) DEFAULT NULL,
  `TELP` varchar(16) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `JABATAN` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PEGAWAI`
--

INSERT INTO `PEGAWAI` (`ID_PGW`, `ID_USER`, `NIP`, `NAMA`, `GENDER`, `TGL_LAHIR`, `ALAMAT`, `TELP`, `EMAIL`, `JABATAN`) VALUES
(1, 1, '1534010032', 'muhammad syafriansyah', '1', '1997-04-03', 'gresik', '084730462830', 'safriansah@gmail.com', 'magang'),
(2, 2, '1534010003', 'wahid', '1', '2018-09-07', 'asda', '999', 'asdad@afsjak', 'magang'),
(3, 3, '1534010096', 'begal', '1', '2018-09-17', 'asdjaksjdka', '91931237', 'asdjfaf@afjaskj', 'magang');

-- --------------------------------------------------------

--
-- Table structure for table `PEMESANAN`
--

CREATE TABLE `PEMESANAN` (
  `ID_PESAN` int(11) NOT NULL,
  `ID_PGW` int(11) DEFAULT NULL,
  `KODE_PESAN` char(10) DEFAULT NULL,
  `PEMESAN` varchar(255) DEFAULT NULL,
  `TELP_PEMESAN` varchar(16) DEFAULT NULL,
  `EMAIL_PEMESAN` varchar(255) DEFAULT NULL,
  `PENGIRIM` varchar(255) DEFAULT NULL,
  `ALAMAT_KIRIM` varchar(255) DEFAULT NULL,
  `PENERIMA` varchar(255) DEFAULT NULL,
  `ALAMAT_PENERIMA` varchar(255) DEFAULT NULL,
  `TGL_KIRIM` date DEFAULT NULL,
  `TOTAL_HARGA` decimal(10,0) DEFAULT NULL,
  `STATUS` char(1) DEFAULT NULL,
  `CREATE_AT` datetime DEFAULT NULL,
  `CREATE_BY` int(11) DEFAULT NULL,
  `UPDATE_AT` datetime DEFAULT NULL,
  `UPDATE_BY` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PEMESANAN`
--

INSERT INTO `PEMESANAN` (`ID_PESAN`, `ID_PGW`, `KODE_PESAN`, `PEMESAN`, `TELP_PEMESAN`, `EMAIL_PEMESAN`, `PENGIRIM`, `ALAMAT_KIRIM`, `PENERIMA`, `ALAMAT_PENERIMA`, `TGL_KIRIM`, `TOTAL_HARGA`, `STATUS`, `CREATE_AT`, `CREATE_BY`, `UPDATE_AT`, `UPDATE_BY`) VALUES
(1, 1, '180906GTGJ', 'jasdkaj', '00000', 'ajdkjaskdj@skdjaskdj', 'kasdjkdjaskd', 'dkjakdjakj', 'kdjakjsdkasjd', 'kdjakdjaksjd', '0999-09-01', '92000', '2', '2018-09-06 06:45:13', 1, '2018-09-06 06:49:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `PERTANYAAN`
--

CREATE TABLE `PERTANYAAN` (
  `ID_PERTANYAAN` int(11) NOT NULL,
  `ID_PGW` int(11) DEFAULT NULL,
  `NAMA` varchar(255) DEFAULT NULL,
  `TELP` varchar(16) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `PERTANYAAN` text,
  `STATUS` char(1) DEFAULT NULL,
  `CREATE_AT` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PERTANYAAN`
--

INSERT INTO `PERTANYAAN` (`ID_PERTANYAAN`, `ID_PGW`, `NAMA`, `TELP`, `EMAIL`, `PERTANYAAN`, `STATUS`, `CREATE_AT`) VALUES
(1, NULL, 'tes', '099', 'coba@k', 'ini web apa ya?', '0', '2018-09-06 07:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

CREATE TABLE `USER` (
  `ID_USER` int(11) NOT NULL,
  `ID_PGW` int(11) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `ROLE` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `USER`
--

INSERT INTO `USER` (`ID_USER`, `ID_PGW`, `USERNAME`, `PASSWORD`, `ROLE`) VALUES
(1, 1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', '1'),
(2, 2, 'wahid', '2b1ddf586a8155d1b59c26c087128380', '3'),
(3, 3, 'begal', '7f376ab32974321c9e1c298269bce5d0', '2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `BERITA`
--
ALTER TABLE `BERITA`
  ADD PRIMARY KEY (`ID_BERITA`),
  ADD UNIQUE KEY `SLUG` (`SLUG`),
  ADD KEY `FK_MEMPOSTING` (`ID_PGW`);

--
-- Indexes for table `GALERI`
--
ALTER TABLE `GALERI`
  ADD PRIMARY KEY (`ID_GALERI`),
  ADD KEY `FK_MENGUPLOAD` (`ID_PGW`);

--
-- Indexes for table `PEGAWAI`
--
ALTER TABLE `PEGAWAI`
  ADD PRIMARY KEY (`ID_PGW`),
  ADD KEY `FK_MEMILIKI` (`ID_USER`);

--
-- Indexes for table `PEMESANAN`
--
ALTER TABLE `PEMESANAN`
  ADD PRIMARY KEY (`ID_PESAN`),
  ADD KEY `FK_MENCATAT` (`ID_PGW`);

--
-- Indexes for table `PERTANYAAN`
--
ALTER TABLE `PERTANYAAN`
  ADD PRIMARY KEY (`ID_PERTANYAAN`),
  ADD KEY `FK_MENJAWAB` (`ID_PGW`);

--
-- Indexes for table `USER`
--
ALTER TABLE `USER`
  ADD PRIMARY KEY (`ID_USER`),
  ADD KEY `FK_MEMILIKI2` (`ID_PGW`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `BERITA`
--
ALTER TABLE `BERITA`
  MODIFY `ID_BERITA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `GALERI`
--
ALTER TABLE `GALERI`
  MODIFY `ID_GALERI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `PEGAWAI`
--
ALTER TABLE `PEGAWAI`
  MODIFY `ID_PGW` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `PEMESANAN`
--
ALTER TABLE `PEMESANAN`
  MODIFY `ID_PESAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `PERTANYAAN`
--
ALTER TABLE `PERTANYAAN`
  MODIFY `ID_PERTANYAAN` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `USER`
--
ALTER TABLE `USER`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `BERITA`
--
ALTER TABLE `BERITA`
  ADD CONSTRAINT `FK_MEMPOSTING` FOREIGN KEY (`ID_PGW`) REFERENCES `PEGAWAI` (`ID_PGW`) ON DELETE CASCADE;

--
-- Constraints for table `GALERI`
--
ALTER TABLE `GALERI`
  ADD CONSTRAINT `FK_MENGUPLOAD` FOREIGN KEY (`ID_PGW`) REFERENCES `PEGAWAI` (`ID_PGW`) ON DELETE CASCADE;

--
-- Constraints for table `PEGAWAI`
--
ALTER TABLE `PEGAWAI`
  ADD CONSTRAINT `FK_MEMILIKI` FOREIGN KEY (`ID_USER`) REFERENCES `USER` (`ID_USER`) ON DELETE SET NULL;

--
-- Constraints for table `PEMESANAN`
--
ALTER TABLE `PEMESANAN`
  ADD CONSTRAINT `FK_MENCATAT` FOREIGN KEY (`ID_PGW`) REFERENCES `PEGAWAI` (`ID_PGW`) ON DELETE CASCADE;

--
-- Constraints for table `PERTANYAAN`
--
ALTER TABLE `PERTANYAAN`
  ADD CONSTRAINT `FK_MENJAWAB` FOREIGN KEY (`ID_PGW`) REFERENCES `PEGAWAI` (`ID_PGW`) ON DELETE SET NULL;

--
-- Constraints for table `USER`
--
ALTER TABLE `USER`
  ADD CONSTRAINT `FK_MEMILIKI2` FOREIGN KEY (`ID_PGW`) REFERENCES `PEGAWAI` (`ID_PGW`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
